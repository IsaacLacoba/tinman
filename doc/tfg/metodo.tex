\chapter{Metodología y herramientas}
\label{chap:metodologias}

\drop{E}{n} este capítulo se tratará de explicar las particularidades de la metodología
de trabajo utilizada durante el desarrollo de este proyecto. Por otra parte, se
enumerarán las tecnologías usadas para desarrollarlo.

\section{Metodología usada en este proyecto}
\label{sec:metodologia-usada-en}
Antes de profundizar acerca de las metodologías que se ha intentado seguir en
este proyecto, es importante poner en contexto al lector.

En este proyecto se han creado una serie de pruebas automáticas sobre el
videojuego que se ha desarrollado. A la hora de crear estas pruebas ha sido
necesario implementar todo una serie de técnicas de instrumentación del motor de
juego. Debido a que era necesario implementar estos mecanismos antes de crear la
prueba, no ha sido posible utilizar \acs{TDD} desde el primer momento en este
proyecto.

Por otra parte, la aplicación de testing automático sobre videojuegos es algo
muy novedoso. Si a eso se le suma el caracter cerrado de la industria, se puede
comprender que existe una complejidad añadida, debido a que las compañías que
aplican testing automático a sus desarrollos no comparten este tipo de
conocimientos.

Por tanto, aunque lo ideal hubiese sido aplicar TDD desde el primer momento,
esto no ha sido posible, de forma que se han ido aplicando técnicas de testing
automático en la medida en que los componentes que daban el soporte necesario
iban siendo implementados.

En cuando a la metodología aplicada, se ha seguido un proceso iterativo e
incremental, en el cuál cada una de las iteraciones comenzaba sobre el trabajo
de la anterior y buscando en todo momento crear ejemplos funcionales. La razón
de usar este tipo de metodología es que un videojuego se presta a crear pequeñas
demos que muestren una determinada carectística, así como a ir añadiendo mas
características a estas demos.

Por ejemplo, en un juego se puede hacer un ejemplo en el que se muestra un
escenario, y después, añadir un personaje. Mas adelante se podría dotar de
movimiento a dicho personaje, tras lo que se podría añadir enemigos, dotarlos de
una \acs{IA} cada vez mas sofisticada, y así sucesivamente.

\section{Desarrollo Ágil}
\label{sec:desarrollo-agil}
En esta sección se expondrán los detalles que definen un proceso de desarrollo
ágil, comenzando con el documento que dio origen a dicho movimiento, el
manifiesto ágil.

\subsection{Manifiesto por el desarrollo ágil del software}
\label{sec:manifiesto-por-el}

En 2001, Kent Beck y otros 16 notables desarrolladores de software, escritores y
consultores(grupo conocido como \emph{la Alianza Ágil}, firmaron el
\emph{Manifiesto por el desarrollo ágil del software}~\cite{manifiesto-agil}. En
él se establecía lo siguiente:

``Estamos descubriendo formas mejores de desarrollar software, por medio de
hacerlo y de dar ayuda a otros para que lo hagan. Ese trabajo nos ha hehco
valorar:
\begin{itemize}
\item Los individuos y las acciones, sobre los procesos y las herramientas.
\item El software que funciona, mas que la documentación exhaustiva.
\item La colaboración con el cliente, y no tanto la negociación del contrato.
\item Responder al cambio, mejor que apegarse a un plan.
\end{itemize}
Es decir, si bien son valiosos los conceptos que aparecen en segundo lugar,
valoramos mas los que aparecen en primer lugar.

Por otra parte, los doce principios del desarrollo ágil~\cite{principios-agiles} son:
\begin{itemize}
\item La mayor prioridad es satisfacer al cliente mediante la entrega temprana y
  continua de software con valor.
\item Se acepta que los requisitos cambien, incluso en etapas tardías del
  desarrollo. Los procesos Ágiles aprovechan el cambio para proporcionar ventaja
  competitiva al cliente.
\item Se entrega software funcional frecuentemente, entre dos semanas y dos
  meses, con preferencia al periodo de tiempo más corto posible.
\item Los responsables de negocio y los desarrolladores trabajan juntos de
  forma cotidiana durante todo el proyecto.
\item Los proyectos se desarrollan en torno a individuos motivados. Hay que
  darles el entorno y el apoyo que necesitan, y confiarles la ejecución del
  trabajo.
\item El método más eficiente y efectivo de comunicar información al equipo de
  desarrollo y entre sus miembros es la conversación cara a cara.
\item El software funcionando es la medida principal de
progreso.
\item Los procesos Ágiles promueven el desarrollo sostenible. Los promotores,
  desarrolladores y usuarios deben ser capaces de mantener un ritmo constante de
  forma indefinida.
\item La atención continua a la excelencia técnica y al buen diseño mejora la
  Agilidad.
\item La simplicidad, o el arte de maximizar la cantidad de trabajo no
  realizado, es esencial.
\item Las mejores arquitecturas, requisitos y diseños emergen de equipos
  auto-organizados.
\item A intervalos regulares el equipo reflexiona sobre cómo ser más efectivo
  para a continuación ajustar y perfeccionar su comportamiento en consecuencia.
\end{itemize}

En las siguientes secciones se desarrollarán cada uno de estos puntos.

\subsection{Proceso ágil}
\label{sec:proceso-agil}

Según~\cite{Pressman2005}, cualquier proceso de software ágil se caracteriza por
la forma en la que aborda cierto número de suposiciones clave acerca de la
mayoría de proyectos de software:

\begin{enumerate}
\item Es difícil predecir qué requerimientos de software persistirán y cuáles
  cambiarán. También es difícil pronosticar cómo cambiarán las prioridades del
  cliente a medida que avanza el proyecto.
\item Para muchos tipos de software, el diseño y la construcción están
  superpuestos; es decir, ambas actividades deben ejecutarse de forma
  simultánea, de modo que los modelos de diseño se prueben a medida que se
  crean. Es difícil predecir cuánto diseño se necesita antes de que se use la
  construcción para probar el diseño.
\item El análisis, el diseño, la construcción y las pruebas no son tan
  predecibles como sería deseable (desde el punto de vista de la planificación).
\end{enumerate}

Dadas estas tres suposiciones, la metodología ágil intenta combatir la
impredecibilidad de los proyectos software mediante la adaptabilidad del proceso
al cambio del proyecto y a las condiciones técnicas. Para lograr esa
adaptabilidad, se requiere un desarrollo incremental del proyecto, apoyando cada
uno de los incrementos de trabajo en la retroalimentación del cliente, de modo
que sea posible hacer las adaptaciones apropiadas. Esto se consigue a través de
la entrega de prototipos o porciones funcionales del sistema en construcción.

\subsection{\acf{TDD}}
\label{sec:test-driv-devel}

El desarrollo de videojuegos es una labor muy susceptible a
errores. Tener pruebas que ayuden a ver que el código que vamos
generando no produce conflictos con el previamente existente es clave
a la hora de desarrollar un producto de mayor calidad.

\acs{TDD} es una metodología ágil de desarrollo software creada por Kent
Beck. Se fundamenta en guiar la creación del software a partir de las pruebas,
típicamente unitarias. En la figura~\ref{fig:flujo-tdd} se muestra el flujo de
trabajo que sigue esta metodología.

\begin{figure}[h]
  \centering
  \includegraphics[width=15cm]{Test-driven_development.PNG}
  \caption[Flujo de trabajo con \acs{TDD}]{Flujo de trabajo con \acs{TDD}\protect\footnotemark}
  \label{fig:flujo-tdd}
\end{figure}
\footnotetext{\url{https://upload.wikimedia.org/wikipedia/commons/9/9c/Test-driven_development.PNG}}
Según~\cite{beck-test-drivendevelopment-2003}, los pasos que sigue esta
metodología son los siguientes:
\begin{enumerate}
\item Añadir un test. En \acs{TDD} cada nueva característica que se quiera
  añadir al sistema comienza con la escritura de un test. Para escribir un test,
  el desarrollador debe entender cláramente las especificaciones y
  requisitos. Esto hace que el desarrollador se centre únicamente en los
  requisitos del proyecto antes de escribir el código, eliminando lo que se
  conoce como \emph{código muerto}

\item Ejecutar todos los test para ver si el mas nuevo de ellos falla. Esto
  permite que el test mas reciente no da un falso positivo. Debido a que todavía
  no se ha escrito el código fuente que hace que el test pase, lo correcto es
  que el test falle, ya que la nueva característica que se quiere probar todavía
  no está implementada. Este paso aumenta la confianza del desarrollador al
  comprobar que el test prueba una característica concreta del código, y pasa
  sólo en los casos previstos.

\item Escribir el código fuente que haga que la prueba pase. En este paso se
  persigue implementar el código fuente necesario para que el test pase. El
  código generado en esta fase no tiene que ser perfecto, pues el objetivo es
  conseguir que el test pase en el menor tiempo posible.

\item Ejecutar los tests. Si todos los test pasas, se corrobora que el código
  cumple con los requisitos que validan las pruebas, además de asegurar que el
  nuevo código fuente no rompe o degrada ninguna de las características previas
  del sistema en construcción. Si algún test falla, se debe corregir el código
  fuente recientemente añadido.

\item Refactorizar el código. La creciente base de código fuente debe ser
  limpiada regularmente durante la aplicación de \acs{TDD}. El código añadido
  puede ser movido a otro lugar mas adecuado, se debe eliminar el código
  duplicado, se debe renombrar todas las clases, objetos, variables, módulos y
  métodos cuyos nombres no reflejen claramente su función. En este paso se
  mejora la legibilidad del código y su mantenibilidad, lo cuál aumenta el valor
  del código fuente a lo largo de su ciclo de vida. Al reejecutar los test, se
  asegura que el proceso de refactorización no altera la funcionalidad
  existente.

  Eliminar código duplicado es importante en el proceso de desarrollo de
  software; sin embargo, en este caso tambien se hace referencia al código
  duplicado en los test, de forma que tambien se pueden reescribir, fusionar o
  eliminar test siempre que se conserve la cobertura de código.

\item Repetir. Comenzando con un nuevo test, el ciclo se repite cada vez que se
  quiera añadir una nueva funcionalidad. Se debe intentar añadir funcionalidad
  en iteraciones lo mas pequeñas posibles. Si el código añadido no satisface un
  nuevo test o los tests anteriores comienzan a fallar sin razón aparente, es
  una buena práctica deshacer los cambios en lugar de invertir tiempo en un
  proceso de depuración lento y costoso. La integración continua ayuda aportando
  un punto de control sobre el que revertir los cambios. Cuando se usan
  bibliotecas externas es importante es tan importante que los incrementos sean
  lo suficientemente grandes como para que no se acabe probando la bibblioteca,
  a menos que se sospeche que no está lo suficientemente probada y contenga
  errores.
\end{enumerate}

\subsection{Ventajas de aplicar \acs{TDD}}
\label{sec:ventajas-de-aplicar}
A raíz de lo anterior, se deducen una serie de ventajas que aporta \acs{TDD}
como metodología de desarrollo:

\begin{itemize}
\item Cuando se escribe el código antes que las pruebas, estas suelen estar
  condicionadas por la funcionalidad implementada, con lo que es fácil obviar
  condiciones en las pruebas. Realizando el proceso a la inversa, se evita este
  problema.
\item Al realizar primero las pruebas se realiza un ejercicio previo de análisis
  en profundidad de los requisitos y de los diversos escenarios. Eliminando la
  mayor parte de variabilidad y encontrado aquellos aspectos mas importantes o
  no contemplados en los requisitos.
\item Debido a que el código fuente implementado es el mínimo imprescindible que
  cumpla la condición de los test, se reduce la redundancia y el \emph{código
    muerto}.
\item Una correcta refactorización del código fuente hace que este sea mas
  legible, óptimo y fácil de mantener.
\item Dado que el código evoluciona con el paso del tiempo, el refactoring debe
  aplicarse, siempre que sea necesario, tanto al código implementado como a las
  pruebas, con el fin de mantenerlas actualizadas, añadiendo nuevos
  casos, cuando sea necesario, o completándolas al detectar fallos en el código.
\end{itemize}


\subsection{Dificultades a la hora de emplear \acs{TDD} en el desarrollo de un
  videojuego}
\label{sec:dificultades-la-hora}

La aplicación de \acs{TDD} a cualquier tipo de proyecto requiere un esfuerzo
adicional, ya que el desarrollador debe pensar en primer lugar qué quiere
probar, antes de pensar cómo lo va a desarrollar. Además, una vez que sepa qué
quiere probar, debe pensar cómo va a comprobar que el software que se está
probando efectivamente realiza la tarea que se espera que realice.

Esto es especialmente duro cuando dicho proyecto trata de desarrollar un
videojuego, ya que comprobar que el software realiza la tarea esperada no es
nada sencillo. Para ilustrarlo, se va a poner un ejemplo.

En un videojuego de carreras, sería razonable realizar pruebas de sistema que
comprueben si la \acs{IA} se comporta como se espera. Por un lado, dependiendo del
tipo de videojuego de carrera, este comportamiento podría variar mucho. Por
otro, el resultado de cada uno de los pasos que aplica el algoritmo de \acs{IA} sobre
un coche se ve plasmado en forma de imágenes, por lo que actualmente no es
posible realizar pruebas directamente sobre las imagenes generadas en un tiempo
razonable~\footnote{El motivo de realizar TDD es poder tener un banco de pruebas
  que permitan detectar lo antes posibles problemas en el software; por tanto,
  es importante ejecutar las pruebas lo mas a menudo posible}. Es cierto, que no
es imposible comprobar una serie de valores concretos que permitan cercionarse
al desarrollador de que la \acs{IA} se comporta de acuerdo a lo que él esperaba.

Por otra parte, aunque sea posible comprobar que lo que una prueba espera está
sucediendo durante la ejecución del juego, hay que señalar que los videojuegos
abusan de cálculos en coma flotante. Los resultados de dichos cálculos
normalmente cambian dependiendo del tipo de
procesador~\cite{floating-point-determinism}; por ejemplo, el juego
\emph{Battlezone 2} usa un modelo de red (\emph{lockstep
  determinism}~\ref{sec:peer-peer-lockstep} que requería que los cálculos que
realizaran los clientes fuesen idénticos hasta el último bit de la mantisa o de
otro modo las simulaciones variaban. Estas desviaciones de los cálculos, (en
cosenos, senos, tangentes y sus inversas), eran debidas a que los procesadores
\emph{Intel} y \emph{AMD} daban resultados diferentes a las mismas operaciones
aritméticas. Para conseguir que los resultados no variasen, tuvieron que
implementar sus propias funciones aritméticas para garantizar que el procesador
no realizase las optimizaciones producían dichas variaciones en los cálculos.

Otro punto que no favorece la aplicación de \acs{TDD} a un videojuego es el
hecho de que normalmente durante la ejecución del mismo hay eventos aleatorios,
que tienen como función mejorar la experiencia del jugador; por ejemplo, que el
comportamiento de la \acs{IA} no sea estático, sino que se adapte a las circunstancias
de la partida. Dado que dichos eventos no se pueden replicar en el momento en el
que se desee, probar que cuando ocurren el videojuego se comporta como se espera
no es fácil, ya que aunque se puede provocar que sucedan dichos eventos, puede
que el resultado no sea el mismo.

A pesar de estas dificultades, las ventajas que aporta TDD las superan
ampliamente, ya que la verdadera dificultad detrás de las enumeradas
anteriormente es que el desarrollador tenga la madurez suficiente como para ser
capaz de superar todas estas dificultades.

\section{Herramientas}
Debido a la cantidad de problemas que se han resuelto durante la realización de
este proyecto, se ha tenido que integrar una gran cantidad de herramientas, que
han ayudado a la resolución de dichos problemas. En esta sección se enumerarán
todas esas herramientas dando una pequeña descripción sobre ellas.

\subsection{Lenguajes de programación}
\label{sec:leng-de-progr}
Se han utilizado diferentes lenguajes de programación atendiendo a cuál se
adaptaba a la hora de aportar soluciones:

\begin{description}
\item[Python] Lenguaje de alto nivel, interpretado y orientado a
  objetos. Típicamente utilizado como lenguaje de prototipado, se ha utilizado
  para la creación de las pruebas de integración del proyecto.
\item[C++] Creado por Bjarne Stroustrup~~\cite{Stroustrup:2013:CPL:518791},
  \acs{C++} proporciona mecanismos de orientación a objetos y compatibilidad con
  C. Es un lenguaje compilado que ofrece distintos niveles de abstracción al
  programador. Debido a su versatilidad y potencia, es el lenguaje estándar que
  utiliza la industria del videojuego y, por tanto, requisito del proyecto.
\item[Bash] \acf{BASH}~\cite{bash} es un lenguaje de shell utilizado para
  realizar diferentes scripts con el fin de automatizar tareas relacionadas con
  la instalación del proyecto y la compilación.
\item[XML] lenguaje de marcas desarrollado por el \acf{W3C} utilizado para
  almacenar datos en un formato inteligible. Se usa para definir la jerarquía de
  los elementos de la interfaz gráfica, así como tambien definir las propiedades
  de cada uno de los widgets utilizados.
\end{description}

\subsection{Control de versiones}
\label{sec:control-de-versiones}

Para el desarrollo del proyecto, se ha optado por usar controles de versiones
para gestionar de forma controlada los cambios del proyecto. Este tipo de
software ofrece creación de ramas de desarrollo, que es una forma de estructurar
las versiones del proyecto, lo cuál permite tener una versión estable, al mismo
tiempo que se sigue desarrollando nuevas caracteristicas en otras ramas.

\begin{description}
\item[Git] Creado por Linus Torbald y distribuido bajo licencia \acs{GPL}
  v.2. Es sistema de control de versiones utilizado para gestionar los cambios
  del proyecto.
\end{description}

\subsection{Herramientas de desarrollo}
\label{sec:herr-de-desarrollo}

A la hora de desarrollar el proyecto se han utilizado una serie de herramientas
tanto para programar, como para automatizar el proceso de compilación.

\begin{description}
\item[Emacs] \acf{EMACS}~\cite{emacs} editor de texto utilizado como \acf{IDE}
  de desarrollo gracias a un plugin llamado
  \emph{emacs-pills}~\cite{emacs-pills}.
\item[Blender]~\cite{blender} programa de modelado y creación de gráficos
  tridimensionales distribuido bajo licencia \acs{GPL}. Este programa se ha
  usado para crear los modelos 3D usados en el juego, realizar el mapeado de las
  texturas y crear animaciones basadas en huesos.
\item[\acs{GIMP}] Editor de imágenes y retoque fotográfico. Se ha utilizado para crear
  elementos del \acs{HUD}.
\item[Make]~\cite{make} Permite la compilación automática del proyecto. El
  proceso de compilación del proyecto está desarrollado con esa herramienta,
  incluido también el presente documento.
\item[Pkg-config]~\cite{pkg-config} facilita la compilacion y el proceso de lincado
  bilbiotecas. Hace uso de unos fichero con extension *.pc donde se definen los
  directorios de cabeceras y de bibliotecas, de forma que ofrece un método
  unificado para listar las dependencias necesarias para compilar el proyecto.
\item[\acs{GCC}-4.9]~\cite{gcc} Compilador de \acs{GNU} para los lenguajes C y C++.
\item[GDB]~\cite{gdb} Depurador para el compilador de \acs{GNU},
  elegido para realizar la depuración del código fuente programado en
  C++ de forma local o remota cuando sea necesario.
\end{description}

\subsection{Bibliotecas del Motor de juego}
\label{sec:bibl-del-motor}

En cuanto al motor de juegos propio que ha emergido a lo largo de este
desarrollo, a continuación se enumeran las bibliotecas que se han utilizado:

\begin{description}
\item [Ogre3D]~\cite{ogre3d} biblioteca de generación de gráficos 3D de
  propósito general, que a lo largo de los años ha ido cogiendo fuerza entre la
  comunidad debido a que se distribuye bajo licencia MIT y a la potencia que
  ofrece. Se ha utilizado como motor de renderizado, delegando las labores de
  gestión de la escena, tales como iluminación y sombreado dinámico, gestión de
  las mallas 3D y operaciones geométricas en el espacio de la escena.
\item[Bullet Physics]~\cite{bullet-web} biblioteca de gestión de físicas y
  colisiones. Multitud de proyectos comerciales, como \emph{Grand Thef Auto} u
  organizaciones como la \acs{NASA}~\cite{bullet-nasa} hacen uso de esta
  biblioteca, razón por la cuál se ha delegado toda la gestión de físicas del
  juego en ella. Ofrece soporte para Windows, Linux, IOS y Android.
\item[OIS]~\cite{OIS-repo} biblioteca de gestión de eventos que ofrece soporte
  para Windows, GNU/Linux, existiendo un port de esta biblioteca para android
  creado por la comunidad de Ogre3D.
\item[CEGUI]~\cite{cegui} biblioteca de gestión de widget gráficos. Se ha
  delegado en ella la gestión de la interfaz gráfica, creación de ventanas y
  menús.
\item[ZeroC Ice]\acs{ICE}\cite{ice} es un middleware de red orientado a
  objetos. Se ha utilizado debido a que facilita enormemente la programación en
  red, además de que añade una sobrecarga mínima a las comunicaciones.
\item[SDL] \acs{SDL} es un conjunto de bibliotecas desarrolladas en lenguaje C
  que ofrece operaciones bácisa de dibujo en 2D, gestión de efectos de sonido y
  música, además de carga y gestión de elementos multimedia.
\item[Boost] es un conjunto de bibliotecas de software libre preparadas para
  extender las capacidades del lenguaje de programacion C++, distribuido bajo
  licencia BSD. Gran parte de las características que ofrecen este conjunto de
  bibliotecas han sido absorbidas por la biblioteca estandar de C++. En este
  proyecto se ha usado la biblioteca de Logs(Boost.Log).
\end{description}

\subsection{Documentación}
\label{sec:documentación}

La realización de documentación se ha llevado a cabo utilizando tanto
herramientas de maquetación de textos como herramientas que permitan
hacer diagramas para ilustrar y facilitar la comprensión del proyecto.

\begin{description}
\item[\acs{GIMP}] Editor de imágenes y retoque fotográfico. Utilizado en la
  maquetación de fotografías e ilustraciones.
\item[Inkscape] Editor de imágenes vectorial.
\item[Libreoffice Impress] Programa para la realización de
  presentaciones, utilizado en algunas de las presentaciones que se
  han realizado de este proyecto.
\item[Dia] Editor de diagramas. Utilizado para construir los diagramas que se
  han utilizado para el desarrollo del proyecto.
\item[\LaTeX] Lenguaje de maquetación de textos, utilizado para la
  elaboración del presente documento y algunas presentaciones.
\item[noweb] Herramienta de programación literal utilizada durante
  algunas de las iteraciones.
\item[arco-pfc] Plantilla de \LaTeX utilizada en la documentación de este proyecto.
\end{description}

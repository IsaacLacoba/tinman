\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {v}}{chapter*.1}
\contentsline {chapter}{Abstract}{\es@scroman {vii}}{chapter*.2}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ix}}{chapter*.3}
\contentsline {chapter}{\'{I}ndice general}{\es@scroman {xiii}}{chapter*.4}
\contentsline {chapter}{\'{I}ndice de cuadros}{\es@scroman {xvii}}{chapter*.7}
\contentsline {chapter}{\'{I}ndice de figuras}{\es@scroman {xix}}{chapter*.8}
\contentsline {chapter}{\IeC {\'I}ndice de listados}{\es@scroman {xxi}}{chapter*.9}
\contentsline {chapter}{Listado de acr\IeC {\'o}nimos}{\es@scroman {xxiii}}{chapter*.10}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Estructura del documento}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Repositorio, Nombre del proyecto y p\IeC {\'a}gina web}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Objetivos}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Objetivo general}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}Objetivos espec\IeC {\'\i }ficos}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Objetivo 1}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Objetivo 2}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Objetivo 3}{6}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Objetivo 4}{6}{subsection.2.2.4}
\contentsline {chapter}{\numberline {3}Antecedentes}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Motores de Juego}{7}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Motores de juegos comerciales}{8}{subsection.3.1.1}
\contentsline {subsubsection}{Unity3D Pro}{8}{subsection.3.1.1}
\contentsline {subsubsection}{Unreal Engine 4}{9}{subsection.3.1.1}
\contentsline {subsubsection}{CryEngine}{9}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Alternativas libres}{10}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Motor de Juegos Libre: Godot}{10}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Ogre3D}{11}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Bullet Physics}{14}{subsection.3.2.3}
\contentsline {subsubsection}{Arquitectura}{15}{subsection.3.2.3}
\contentsline {subsubsection}{Encauzamiento del componente de f\IeC {\'\i }sica de cuerpos r\IeC {\'\i }gidos}{15}{figure.3.3}
\contentsline {section}{\numberline {3.3}F\IeC {\'\i }sicas en din\IeC {\'a}mica de veh\IeC {\'\i }culos}{16}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Movimientos rectil\IeC {\'\i }neos}{16}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Transferencia de peso}{18}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Giros}{18}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Visi\IeC {\'o}n general de la Inteligencia Artificial}{19}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Algoritmo A*}{21}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Rubber Band}{23}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Caso de estudio: \acf {TORCS}}{24}{section.3.5}
\contentsline {section}{\numberline {3.6}Historia de los juegos en red}{26}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Peer-to-Peer Lockstep}{27}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Cliente-Servidor}{28}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Mecanismo de predicci\IeC {\'o}n en el lado del cliente}{29}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}Compensaci\IeC {\'o}n de latencia}{31}{subsection.3.6.4}
\contentsline {section}{\numberline {3.7}T\IeC {\'e}cnicas de sincronizaci\IeC {\'o}n en videojuegos}{32}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Lockstep determinista}{33}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Snapshots e Interpolaci\IeC {\'o}n}{34}{subsection.3.7.2}
\contentsline {subsubsection}{Tipos de interpolaci\IeC {\'o}n}{34}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Sincronizaci\IeC {\'o}n basada en estado}{36}{subsection.3.7.3}
\contentsline {section}{\numberline {3.8}Middleware orientado a objetos}{37}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}ZeroC \acs {ICE}}{38}{subsection.3.8.1}
\contentsline {subsubsection}{Adaptador de objetos}{39}{figure.3.18}
\contentsline {subsubsection}{Sirviente}{39}{figure.3.18}
\contentsline {subsubsection}{Proxy}{39}{figure.3.18}
\contentsline {subsubsection}{Communicator}{39}{figure.3.18}
\contentsline {subsubsection}{Slice}{39}{figure.3.18}
\contentsline {subsubsection}{Servicios}{40}{lstnumber.3.4.6}
\contentsline {subsection}{\numberline {3.8.2}CORBA}{42}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Java RMI}{42}{subsection.3.8.3}
\contentsline {section}{\numberline {3.9}T\IeC {\'e}cnicas en desarrollo de videojuegos}{43}{section.3.9}
\contentsline {subsection}{\numberline {3.9.1}Testing autom\IeC {\'a}tico en videojuegos}{47}{subsection.3.9.1}
\contentsline {chapter}{\numberline {4}Desarrollo del proyecto}{51}{chapter.4}
\contentsline {section}{\numberline {4.0}Entrenamiento con la biblioteca de renderizado}{51}{section.4.0}
\contentsline {subsection}{\numberline {4.0.1}Analisis}{51}{subsection.4.0.1}
\contentsline {subsection}{\numberline {4.0.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{52}{subsection.4.0.2}
\contentsline {section}{\numberline {4.1}Creaci\IeC {\'o}n de la primera prueba y renderizado de un coche}{53}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}An\IeC {\'a}lisis}{54}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{55}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Entrenamiento con la biblioteca de gesti\IeC {\'o}n de colisiones}{58}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}An\IeC {\'a}lisis}{58}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{60}{subsection.4.2.2}
\contentsline {subsubsection}{Integraci\IeC {\'o}n con Ogre3D}{60}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Gesti\IeC {\'o}n de eventos de usuario}{62}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}An\IeC {\'a}lisis}{62}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{63}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Modelado realista del movimiento del coche}{67}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}An\IeC {\'a}lisis}{68}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{69}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Creaci\IeC {\'o}n de la m\IeC {\'a}quina de estados y de las principales interfaces}{77}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}An\IeC {\'a}lisis}{77}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{79}{subsection.4.5.2}
\contentsline {subsubsection}{Creaci\IeC {\'o}n de las primeras interfaces}{79}{subsection.4.5.2}
\contentsline {subsubsection}{Definici\IeC {\'o}n e implementaci\IeC {\'o}n de la \acs {SFM}}{82}{lstnumber.4.18.2}
\contentsline {section}{\numberline {4.6}Creaci\IeC {\'o}n del circuito}{85}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}An\IeC {\'a}lisis}{85}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{86}{subsection.4.6.2}
\contentsline {section}{\numberline {4.7}Adici\IeC {\'o}n de mec\IeC {\'a}nicas de juego y mejora visual del videojuego}{91}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}An\IeC {\'a}lisis}{92}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{93}{subsection.4.7.2}
\contentsline {section}{\numberline {4.8}Implementaci\IeC {\'o}n del modelo de red}{95}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}An\IeC {\'a}lisis}{95}{subsection.4.8.1}
\contentsline {subsection}{\numberline {4.8.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{97}{subsection.4.8.2}
\contentsline {subsection}{\numberline {4.8.3}An\IeC {\'a}lisis del ancho de banda consumido}{107}{subsection.4.8.3}
\contentsline {section}{\numberline {4.9}Implementaci\IeC {\'o}n del algoritmo de inteligencia artificial}{108}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}An\IeC {\'a}lisis}{108}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{109}{subsection.4.9.2}
\contentsline {section}{\numberline {4.10}Instrumentaci\IeC {\'o}n del videojuego}{112}{section.4.10}
\contentsline {subsection}{\numberline {4.10.1}An\IeC {\'a}lisis}{112}{subsection.4.10.1}
\contentsline {subsection}{\numberline {4.10.2}Dise\IeC {\~n}o e Implementaci\IeC {\'o}n}{113}{subsection.4.10.2}
\contentsline {section}{\numberline {4.11}An\IeC {\'a}lisis de costes}{120}{section.4.11}
\contentsline {chapter}{\numberline {5}Arquitectura del Motor}{123}{chapter.5}
\contentsline {section}{\numberline {5.1}Clase Game}{123}{section.5.1}
\contentsline {section}{\numberline {5.2}Gestores de utilidad}{124}{section.5.2}
\contentsline {section}{\numberline {5.3}Jerarqu\IeC {\'\i }a de estados}{125}{section.5.3}
\contentsline {section}{\numberline {5.4}Controladores de jugador}{126}{section.5.4}
\contentsline {section}{\numberline {5.5}Clase Sesi\IeC {\'o}n}{127}{section.5.5}
\contentsline {section}{\numberline {5.6}Clase Car}{127}{section.5.6}
\contentsline {chapter}{\numberline {6}Metodolog\IeC {\'\i }a y herramientas}{129}{chapter.6}
\contentsline {section}{\numberline {6.1}Metodolog\IeC {\'\i }a usada en este proyecto}{129}{section.6.1}
\contentsline {section}{\numberline {6.2}Desarrollo \IeC {\'A}gil}{130}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Manifiesto por el desarrollo \IeC {\'a}gil del software}{130}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Proceso \IeC {\'a}gil}{131}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}\acf {TDD}}{131}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Ventajas de aplicar \acs {TDD}}{133}{subsection.6.2.4}
\contentsline {subsection}{\numberline {6.2.5}Dificultades a la hora de emplear \acs {TDD} en el desarrollo de un videojuego}{134}{subsection.6.2.5}
\contentsline {section}{\numberline {6.3}Herramientas}{135}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Lenguajes de programaci\IeC {\'o}n}{135}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Control de versiones}{135}{subsection.6.3.2}
\contentsline {subsection}{\numberline {6.3.3}Herramientas de desarrollo}{136}{subsection.6.3.3}
\contentsline {subsection}{\numberline {6.3.4}Bibliotecas del Motor de juego}{136}{subsection.6.3.4}
\contentsline {subsection}{\numberline {6.3.5}Documentaci\IeC {\'o}n}{137}{subsection.6.3.5}
\contentsline {chapter}{\numberline {7}Conclusiones y trabajo futuro}{139}{chapter.7}
\contentsline {subsection}{\numberline {7.0.6}Conclusiones}{139}{subsection.7.0.6}
\contentsline {subsection}{\numberline {7.0.7}Mejoras y trabajo futuro}{140}{subsection.7.0.7}
\contentsline {chapter}{\numberline {A}Manual de usuario. Instalaci\IeC {\'o}n del juego}{143}{appendix.Alph1}
\contentsline {chapter}{\numberline {B}GNU Free Documentation License}{145}{appendix.Alph2}
\contentsline {section}{\numberline {B.0}PREAMBLE}{145}{section.Alph2.0}
\contentsline {section}{\numberline {B.1}APPLICABILITY AND DEFINITIONS}{145}{section.Alph2.1}
\contentsline {section}{\numberline {B.2}VERBATIM COPYING}{146}{section.Alph2.2}
\contentsline {section}{\numberline {B.3}COPYING IN QUANTITY}{146}{section.Alph2.3}
\contentsline {section}{\numberline {B.4}MODIFICATIONS}{147}{section.Alph2.4}
\contentsline {section}{\numberline {B.5}COLLECTIONS OF DOCUMENTS}{148}{section.Alph2.5}
\contentsline {section}{\numberline {B.6}AGGREGATION WITH INDEPENDENT WORKS}{148}{section.Alph2.6}
\contentsline {section}{\numberline {B.7}TRANSLATION}{148}{section.Alph2.7}
\contentsline {section}{\numberline {B.8}TERMINATION}{148}{section.Alph2.8}
\contentsline {section}{\numberline {B.9}FUTURE REVISIONS OF THIS LICENSE}{149}{section.Alph2.9}
\contentsline {section}{\numberline {B.10}RELICENSING}{149}{section.Alph2.10}
\contentsline {chapter}{Referencias}{151}{appendix*.13}

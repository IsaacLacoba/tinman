En esta iteración se pretende crear las primeras interfaces gráficas que
permitan la interacción del jugador con el videojuego. El objetivo que se persigue en
construir unas interfaces lo mas sencillas posible, sobre las que se pueda ir
añadiendo mas elementos a lo largo del desarrollo.

Además, se intentará controlar el flujo de ejecución del videojuego a través de
la creación de una \acf{SFM} que permita abstraer los detalles de mas
bajo nivel.

\subsection{Análisis}
\label{sec:analisis-5}

Como se ha dicho, uno de los objetivos de esta iteración consiste en crear una
interfaz gráfica lo suficientemente simple como para cubrir las necesidades de
interacción del jugador con el videojuego. Los elementos mínimos que se
requieren para crear dicha interfaz son:
\begin{itemize}
\item Una serie de botones que permitan cambiar entre las pantallas del
  videojuego. Por ejemplo, del menú principal a la pantalla donde se inicia el
  juego o hacia la pantalla de créditos.
\item Elementos que permitan agrupar dichos botones (subventanas). Por ejemplo, durante el
  juego, se dará la opción al jugador de pausarlo pulsando el botón
  \emph{Escape}, lo que hará que aparezca una pantalla de pausa con una serie de
  botones.
\item Cuadros de texto (\emph{text input}) que permitan añadir información como
  el \emph{nickname} del jugador o la información de red para poder conectarse
  al servidor en la partida (IP del servidor cuando se implemente el
  componente de red)
\item Checkboxes; por ejemplo, para activar o silenciar la música del juego.
\item Barras de progreso.
\end{itemize}

Por otra parte, se hace necesario implementar un mecanismo de alto nivel que
permita cambiar entre diferentes pantallas del videojuego. Cada una de estas
pantallas tendrá información que será única de ellas, con condiciones
específicas para cambiar de una a otra y siempre dependiendo de que se
cumplan dichas condiciones; por ejemplo, para cambiar de la pantalla del menú a
la de juego es necesario que el jugador pulse el botón \emph{Jugar}. Del mismo
modo, no se puede cambiar desde la pantalla de juego hacia la de menú sin pasar
por la pantalla de \emph{Pausa} y para esto el jugador debe pulsar el botón
\emph{Escape}. Por tanto, parece claro que se puede modelar el flujo de
ejecución del videojuego como una \acs{SFM}.

Al implementar los cambios de estados del juego mediante una maquina de estados
se persigue desacoplar la lógica del \emph{bucle de juego}. El \emph{bucle de
  juego} es la estructura de control principal de todo videojuego desde la que
se ejecutan todas las demás instrucciones. En el
listado~\ref{list:bucle-no-estados} se puede ver el pseudocódigo de un
\emph{bucle de juego} típico en el que no se hace uso de una máquina de estados
para controlar el flujo de ejecución:


\begin{listing}[
  language = C++,
  caption = {Pseudocódigo del bucle de juego sin orientación a estados},
  label = list:bucle-no-estados]
Mientras !game.shutdown:
  CapturarEventosEntrada()

  deltaT += get_delta_time()
  Si Pantalla == Menu
    (Logica de la pantalla Menu)
  Si Pantalla == Juego
    (Logica de la pantalla Juego)
  Si Pantalla == Pausa
    (Logica de la pantalla Pausa)
  Si Pantalla == Resultados
    (Logica de la pantalla Resultados)
  //etcetera

  RenderizarEscena()
\end{listing}

Acoplar la lógica específica de cada estado al bucle de juego hace que este no
sea mantenible ni flexible, ya que se generan numerosos problemas al querer
modificar dicha estructura:
\begin{itemize}
\item difícil lectura y comprensión del codigo fuente,
\item mayor complejidad a la hora de realizar modificaciones,
\item aumento del número de horas de desarrollo en siguientes iteraciones,
\item mayor dificultad a la hora de depuración,
\item etcétera.
\end{itemize}

De esta forma, se identifican dos problemas a los que se ha dado solución en
esta iteración:
\begin{itemize}
\item Encontrar una biblioteca de widget gráficos que ayude a implementar las
  interfaces.
\item Implementar un mecanismo de alto nivel que ayude a desacoplar la lógica de
  cambio de pantallas del bucle de juego.
\end{itemize}

\subsection{Diseño e Implementación}
\label{sec:implementacion-5}
A partir del análisis inicial se dió paso a la implementación de la máquina de
estados y de las primeras interfaces.
\subsubsection{Creación de las primeras interfaces}
\label{sec:creacion-de-las}

A la hora de seleccionar una biblioteca de widgets gráficos se ha elegido
CEGUI~\cite{cegui-web}. CEGUI es una biblioteca orientada a objetos distribuida
bajo licencia MIT. Está escrita en C++. Ofrece soporte para Windows, Linux y
MacOS.

La razón de la elección radica en dos razones. Por un lado, CEGUI cubre todas
las necesidades expuestas anteriormente: da soporte para creación de ventanas,
botones, text-inputs, checkboxes, progress bar, etcétera. Por otro, CEGUI fue la
biblioteca de widgets elegida por el proyecto Ogre3D para crear las interfaces
de los ejemplos que usa para mostrar las características de que dispone el motor
de renderizado. CEGUI ha demostrado ser la opción mas completa a la hora de
gestionar widgets gráficos de todas las alternativas de software libre que se
han expuesto.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\textwidth]{primera-interfaz.png}
  \caption[Resultado de la iteracion 5]{Resultado de la iteracion 5. A la izquierda se ve una captura del menú
    principal, a la derecha una captura del menú de pausa}
  \label{fig:primera-interfaz}
\end{figure}

Se ha creado una clase que hace mas sencillo trabajar con CEGUI, así como crear
Overlays de Ogre3D, cuyo nombre es \emph{GUI}. Un Overlay es un elemento 2D que
se utiliza a la hora de crear elementos decorativos en la interfaz, como iconos,
imágenes de fondo e incluso barras de vida y elementos similares. Esta clase se
trata de un wrapper de CEGUI y Ogre3D, mas sencillo de usar que las bibliotecas
originales.

En la figura~\ref{fig:primera-interfaz} se puede observar el resultado de esta
primera parte de la iteración. Se ha creado una ventana que cuenta con dos
botones, uno para salir de la partida y otro para iniciar el juego, y otra
ventana para pausarlo. A continuación se va a exponer el proceso de creación de
la ventana del menú principal, ya que el proceso es similar para cualquier otra.

A la hora de añadir una ventana al menú del juego, se crea un script con
extensión \emph{.layout}. En el listado~\ref{menu.layout} se puede ver el código
correspondiente al menú principal. La estructura del fichero es la típica de un
fichero \acs{XML}. Cada elemento cuenta con una serie de propiedades, tales como la
orientación vertical u horizontal, el tamaño, la tipografía, el texto, la
posición, el tamaño del elemento, etcétera. En este fichero se define un
contenedor, que contendrá todos los elementos de la ventana. Como hijos de él se
crean un elemento de tipo Texto y dos botones, uno con el texto \emph{Play} y
otro con \emph{Exit}.

\begin{listing}[
  language = xml,
  caption = {Definición de una ventana usando CEGUI},
  label = menu.layout]
  <?xml version="1.0" encoding="UTF-8"?>
  <GUILayout version="4">

  <Window name="Menu" type="TaharezLook/MenuContainer">
  <Property name="VerticalAlignment" value="Centre" />
  <Property name="HorizontalAlignment" value="Centre" />
  <Property name="AlwaysOnTop" value="True" />
  <Property name="Size" value="{{0.4,0},{0.3,0}}" />

  <Window name="Title" type="TaharezLook/Text">
  <Property name="VerticalAlignment" value="Top" />
  <Property name="HorizontalAlignment" value="Centre" />
  <Property name="Size" value="{{0.5,0},{0.30,0}}" />
  <Property name="Position" value="{{0.04,0},{0,0}}" />
  <Property name="Font" value="SoulMission-40" />
  <Property name="Text" value="Tinman" />
  </Window>

  <Window name="Play" type="TaharezLook/MenuItem">
  <Property name="ID" value="1" />
  <Property name="VerticalAlignment" value="Centre" />
  <Property name="HorizontalAlignment" value="Centre" />
  <Property name="Size" value="{{0.3, 0.1},{0.1, 0}}" />
  <Property name="Position" value="{{0,0},{-0.1,100}}" />
  <Property name="Font" value="Manila-12" />
  <Property name="Text" value="Play" />
  </Window>

  <Window name="Exit" type="TaharezLook/MenuItem">
  <Property name="ID" value="1" />
  <Property name="VerticalAlignment" value="Bottom" />
  <Property name="HorizontalAlignment" value="Centre" />
  <Property name="Size" value="{{0.3, 0.1},{0.1, 0}}" />
  <Property name="Font" value="Manila-12" />
  <Property name="Text" value="Exit" />
  </Window>
  </Window>
  </GUILayout>
\end{listing}
Tras esto se debe crear un script que defina el tipo de
letra (\emph{SoulMission-40}). En el listado~\ref{cegui-tipografía} se puede
observar la forma de definir una fuente de letra en CEGUI.

\begin{listing}[
  language = xml,
  caption = {Definición de una tipografía en CEGUI},
  label = cegui-tipografía]
  <?xml version="1.0" ?>
  <Font version="3" name="SoulMission-40" filename="SoulMission.ttf"
  type="FreeType" size="40" nativeHorzRes="1280" nativeVertRes="720" autoScaled="vertical"/>
\end{listing}


Hecho esto, falta inicializar CEGUI y configurar algunos aspectos que no están
reflejados en los scripts mostrados. Para inicializar CEGUI hay que ejecutar la
siguiente instrucción \emph{CEGUI::OgreRenderer::bootstrapSystem()}. CEGUI
permite operar con diversos motores de renderizado(Irrlicht3D, Ogre3D) o
bibliotecas gráficas (OpenGL o DirectX). La instrucción anterior inicializa la
implementación de CEGUI que aporta soporte para interaccionar con Ogre3D.

Una vez que está inicializado el componente principal de CEGUI, el siguiente
paso consiste en inicializar los gestores de recursos por defecto. Estos
gestores se encargarán de cargar y gestionar los principales scripts de
CEGUI. En el listado~\ref{cegui-resource-managers} se muestra el código
necesario para inicializar los gestores por defecto.

\begin{listing}[
  language = xml,
  caption = {Inicialización de los gestores de recursos por defecto de CEGUI},
  label = cegui-resource-managers]
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
\end{listing}

Lo siguiente es cargar el fichero que define las ventana que se van a mostrar y
en configurarla como la ventana pricipal. En el listado~\ref{cargar-ventana} se
muestran las sentencias que realizan estas acciones.

\begin{listing}[
  language = C++,
  caption = {Configuración de la ventana principal},
  label = cargar-ventana]
  std::string file_name = ``Menu.layout''
  CEGUI::Window* window = game_->gui->load_layout(file_name);
  game_->gui_->get_context().setRootWindow(window);
\end{listing}

Una vez que se ha cargado y configurado la ventana principal, se tiene que
definir los callbacks que serán ejecutados cuando los botones de dicha ventana
sean pulsados. El código que asocia las funciones de retrollamada con un botón
se puede ver en el listado~\ref{cegui-callback-boton}. En dicho listado se
muestra el botón de salir del juego del menu principal, al que se asocia como
callback la función que cierra el juego.

\begin{listing}[
  language = C++,
  caption = {Mapeo de un botón de la interfaz con un callback},
  label = cegui-callback-boton]
  std::string file_name = ``Menu.layout''
  CEGUI::Window* window = game_->gui->load_layout(file_name);
  window->getChild(``Exit'')->subscribeEvent(CEGUI::PushButton::EventClicked,
                              CEGUI::Event::Subscriber(&Game::shutdown, game_));
\end{listing}

En el listado~\ref{cegui-callback-boton} se accede al elemento ``Exit'' de la
ventana definida en el fichero \emph{Menu.layout}(ver
listado~\ref{menu.layout}). A este elemento se le suscribe un evento de tipo
pulsación. Cuando se produzca un evento de este tipo(es decir, cuando se pulse
el botón), se ejecutará la función \texttt{Game::shutdown()}, de la instancia
\texttt{game\_}. CEGUI implementa sus propios adaptadores a función, mecanismo
similar al que facilita la biblioteca estándar de C++11

El último paso consiste en injectar el delta de tiempo trascurrido desde la
última vez que se renderizó el juego. En el listado~\ref{cegui-inject-delta} se
pueden ver las instrucciones necesarias.

\begin{listing}[language = C++,
  caption = {Injección del pulso de tiempo a CEGUI},
  label = cegui-inject-delta]
  float delta;
  game_->gui_->inject_delta(delta);
\end{listing}

El resultado de este proceso se puede observar en la rama \emph{guiPhysics} de
este repositorio:
\url{https://goo.gl/q6j3co}.

\subsubsection{Definición e implementación de la \acs{SFM}}
\label{sec:defin-e-impl}

Para la creación de la \acs{SFM}, se asocia cada estado con cada una de las
ventanas que aparecerán. En total, se pueden definir un total de 4 estados para
esta iteración: Menu, Jugar, Pausa y Resultados. En la
figura~\ref{fig:maquina-iteracion5} se define la \acs{SFM} correspondiente a
esta iteración. El estado inicial es \emph{Menu}. Este estado no guarda
información, simplemente sirve como puente entre el estado \emph{Jugar} y el
estado \emph{Salir}. Al pulsar el botón \emph{Salir} se realiza una salida
controlada del videojuego, liberando los recursos adquiridos. Al pulsar el botón
\emph{Play} se cambia al estado \emph{Jugar}. Este estado guarda información
acerca de la partida: el estado de los jugadores y de sus coches. Desde el
estado \emph{Jugar} se puede ir al estado \emph{Pausa} al pulsar la tecla
\emph{Escape}. El estado de pausa detiene la simulación física, de modo que el
estado del mundo se congela hasta que se vuelve al estado Jugar. Desde el estado
Jugar tambien se puede ir al estado de \emph{Resultados}. En este estado se
muestran los resultados de la carrera.  \input{graphs/primera_maquina.tex}

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{jerarquia-estados.png}
  \caption{Jerarquía de estados}
  \label{fig:jerarquía-estados}
\end{figure}


En la figura~\ref{fig:jerarquía-estados} se puede ver un diagrama de la
estructura de las clases de los estados. Se ha definido una jerarquía de
herencia; de esta forma, se puede trabajar con una representación abstracta de
la clase estado, y después implementar las particularidades de cada estado por
separado. Esta modificación permite solucionar el segundo problema. Se plantea
una modificación a la estructura de la clase \emph{Game} que existía hasta este
momento. Hasta ahora, se acoplaba la lógica específica de cada estado al bucle
de eventos.

Con la implementación de la jerarquía de estados, la nueva estructura del bucle
de eventos se puede ver en el listado~\ref{bucle-juego-estados}, que se puede
encontrar en el fichero \emph{game.cpp}. Al invocar el metodo \emph{start}, se
inicia el proceso de configuración de la clase Game. Este proceso se reduce a
crear punteros inteligentes de cada uno de los estados y añadirlos a un mapa. La
clave de cada entrada del mapa es una cadena de texto con el nombre del estado
correspondiente. Tras esto, se invoca al bucle de juego, que se ejecutará hasta
que se cumpla la condición de finalización del juego. El bucle de juego arranca
el timer y en cada iteración del bucle se actualiza el delta de tiempo, se
capturan los eventos de entrada generados por el usuario y se comprueba si el
intervalo de tiempo desde la última vez que se renderizó la escena es mayor que
uno entre el número de frames por segundo que se quiere alcanzar como
máximo. Con esto se trata de renderizar una única vez la escena por cada
intervalo. Por ejemplo, si se quiere renderizar a un ratio de 60 frames/segundo,
se calcula que el delta se igual a 1/60 segundos (16.6ms) y se renderiza una vez
la escena. De esta forma nunca se renderizarán mas de 60 veces por segundo,
ahorrando recursos de la máquina.

Si se cumple la tasa de rendizado por segundo, se comprueba si alguno de los
eventos de entrada que haya generado el usuario, a través del teclado o del
ratón, tenga una acción asociada y, de ser así, se ejecutaría. Después se
ejecuta el método \texttt{update()} del estado actual. Por último, se renderiza la
escena y se actualiza el valor de la variable \emph{delta} a cero.

Cada estado tiene su propia lógica concreta de modo que si se quiere modificar
ésta estará localizada en la clase que represente el estado en cuestión.

\begin{listing}[language = C++,
  caption = {Pseudocódigo del bucle de juego haciendo uso de estados},
  label = bucle-juego-estados]
  void
  Game::start() {
    state_table_["menu"] = std::make_shared<Menu>(shared_from_this());
    state_table_["pause"] = std::make_shared<Pause>(shared_from_this());
    state_table_["play"] = std::make_shared<Play>(shared_from_this());
    state_table_["results"] = std::make_shared<Results>(shared_from_this());

    set_current_state("menu");

    game_loop();
  }

  void
  Game::game_loop() {
    timer_.start();
    while(!exit_) {
      delta_ += timer_.get_delta_time();
      input_->capture();
      if(delta_ >= (1/FPS)) {
        input_->check_events();
        state_table_[current_state_]->update();
        scene_->render_one_frame();
        delta_ = 0.f;
      }
    }
  }
\end{listing}

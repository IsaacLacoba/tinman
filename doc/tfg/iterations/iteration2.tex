En esta iteración se pretende crear un ejemplo mínimo que sirva para
adquirir la destreza suficiente a la hora de trabajar con \emph{Bullet
Physics}, la biblioteca de físicas y gestión de colisiones utilizada
en este proyecto.

\subsection{Análisis}
\label{sec:analisis-2}

Ya se introdujo \emph{Bullet Physics} en la
sección~\ref{sec:bullet-physics}. Bullet es una biblioteca compleja,
con una gran variedad de funcionalidades que permiten modelar
comportamientos físicos basados en las leyes de Newton. En esta
sección se va a hablar de los conceptos que se deben conocer a la hora
de trabajar con este motor de colisiones:

\begin{itemize}
\item El elemento mas importante en Bullet es el \emph{Mundo}. El \emph{Mundo}
  dentro de Bullet tiene varias responsabilidades, entre las que se pueden
  destacar: servir como estructura de datos donde almacenar los cuerpos físicos
  que lo conforman y aplicar una serie de restricciones a estos cuerpos, como la
  fuerza de la gravedad, detectar y aplicar colisiones entre estos cuerpos y
  actualizar su posición automáticamente cuando se aplique cualquier tipo de
  fuerza sobre estos. El \emph{Mundo} tiene diversas implementaciones dentro de
  la bibilioteca, dependiendo de si utilizamos cuerpos rígidos o fluidos.

\item Como se ha dicho, el mundo físico esta compuesto por \emph{cuerpo
    físicos}. Un cuerpo físico entra dentro de las dos siguientes
  categorías: cuerpo rígido o fluido. Un cuerpo rígido se caracteriza
  por tener una forma inmutable. Si se aplica alguna fuerza sobre un
  cuerpo rígido, su forma no variará. En contrapunto, los cuerpos
  fluidos se caracterizan por tener formas de colisión que pueden
  variar cuando se aplica una fuerza sobre estas. Los cuerpos rígidos
  tienen comportamientos menos realistas que los cuerpos fluidos; sin
  embargo, simular un cuerpo fluido es computacionalmente mas complejo
  que simular un cuerpo rígido, debido a las implicaciones que tiene
  la variabilidad de su forma física. Los motores de colisión de
  última generación empiezan a integrar simulación de cuerpos
  fluidos. En particular, el motor \emph{Physx} de \emph{Nvidia}
  cuenta con la posibilidad de simular fluidos. La versión 3 de Bullet
  Physics cuenta con simulación de cuerpos fluidos, aunque todavía
  está en desarrollo y no alcanza un rendimiento aceptable como para
  llevarlo a un videojuego.

\item Los cuerpos físicos cuentan con una \emph{forma de colisión}. Típicamente,
  una forma de colisión suele ser un cuerpo convexo con unas determinadas
  propiedades en función de su forma geométrica, un coeficiente de fricción, de
  restitución, una inercia, etcétera. Bullet ofrece una cuantas formas
  primitivas de colisión:
  \begin{itemize}
  \item btBoxShape: caja definida por el tamaño de sus lados.
  \item btSphereShape: esfera definida por su radio.
  \item btCapsuleShape: capsula alrededor del eje Y. También existen btCapsuleShapeX/Z
  \item btCylinderShape:
  \item btConeShape: cono alrededor del eje Y. También existen
    btConeShapeX/Z.
  \item btMultiSphereShap: cascarón convexo formado a partir de varias
    esferas que puede ser usado para crear una capsula ( a partir de dos
    esferas) u otras formas convexas.
  \end{itemize}
  Bullet también ofrece formas compuestas, pudiendo combinar múltiples
  formas convexas en una única. Cada una de las formas que forman la
  malla principal se llama \emph{forma hija}. En la
  figura~\ref{fig:formas-colision} se muestran las formas de colisión
  de un vehículo (\emph{btBoxShape}) y del suelo (\emph{btBvhTriangleMeshShape})

\item Adicionalmente, se tiene el problema de conectar los cuerpos gráficos,
  pertenecientes al motor de renderizado (Ogre3D) con los cuerpos
  físicos. Es importante señalar que el motor de renderizado y el
  motor físico son componentes del motor de juego totalmente
  independientes que, a priori, no cuentan con ningún mecanismo de
  comunicación entre ellos. La situación es la siguiente: supongamos
  que se quiere renderizar una esfera a una cierta altura sobre el
  suelo. En primer lugar habrá que indicarle al motor de renderizado
  cómo está compuesta la escena que se pretende crear. Por tanto, se
  le indicará que cree el suelo en una posición determinada y una
  esfera, encima del anterior.

  La función de los cuerpos gráficos es únicamente la de ser
  renderizados, formando las imágenes de la escena, pero no tienen
  ningún comportamiento asociado mas allá de esto. Aquí es donde entra
  en acción los cuerpos físicos. Estos sí que tienen un comportamiento
  realista, que permite que los cuerpos interaccionen entre si. Por
  tanto, se deberá indicar al motor de físicas que cree un cuerpo
  rígido con una forma de colisión en forma de plano, de un
  determinado tamaño y en una determinada posición. Además, se le
  indicará que cree un cuerpo físico con forma esférica de un
  determinado radio y a una determinada posición. Si ejecutamos este
  ejemplo, y suponiendo que existe una fuerza de gravedad que atraerá
  la esfera hacia el suelo, ocurrirá lo siguiente: el cuerpo gráfico
  de la esfera no se moverá de su posición inicial. Esto es así debido
  a que el motor de fisicas no tiene ninguna forma de indicarle al
  motor de renderizado las posiciones actualizadas de la esfera en los
  siguientes frames.
\end{itemize}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{formas-colision.png}
  \caption{Renderizado de formas físicas de colisión}
  \label{fig:formas-colision}
\end{figure}


\subsection{Diseño e Implementación}
\label{sec:implementacion-2}

El resultado de esta iteración esta plasmado en el siguiente repositorio:
\url{https://bitbucket.org/IsaacLacoba/intro-bullet}. Además, enq la figura~\ref{fig:simulacion-esferas} se puede ver una captura de pantalla del ejemplo disponible en el repositorio anterior. En él, se crea un plano, y por cada pulsación de la tecla B se crea una esfera, que rebotará contra el suelo. Si
se pulsa rápidamente se puede observar cómo las pelotas rebotan entre
sí, comprobando como el motor de físicas simula perfectamente el
comportamiento de un cuerpo esférico.

\subsubsection{Integración con Ogre3D}
\label{sec:integr-con-ogre3d}

Para resolver el problema de la integración entre \emph{Bullet Physics} y
\emph{Ogre3D}, los cuerpos rígidos de Bullet cuentan con un atributo llamado
\emph{estado de movimiento}(\emph{Motion State}). Este \emph{Motion State}
abstrae de las operaciones de bajo nivel que se realizan cuando el cuerpo físico
recibe la influencia de alguna fuerza. De esta forma, se puede trabajar
directamente con los cuerpos físicos, sabiendo que las posiciones de los
elementos de la escena seran adecuadamente actualizadas en función del
movimiento del cuerpo físico.

En resumen, la clase \emph{MotionState} nos permite actualizar la posición de
los nodos de escena de Ogre. Para ello, se requiere aportar una implementación,
dado que se trata de una clase abstracta.

\begin{listing}[language=C++,
caption = {Método setWorldTransform de la clase MotionState},
label = {list:motion-state}]
    void MotionState::setWorldTransform(const btTransform &worldTrans)
    {
        if(mSceneNode == nullptr)
            return; // silently return before we set a node

        btQuaternion rot = worldTrans.getRotation();
        mSceneNode ->setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
        btVector3 pos = worldTrans.getOrigin();
        mSceneNode ->setPosition(pos.x(), pos.y(), pos.z());
    }
\end{listing}

La clase tiene como únicos atributos un objeto \texttt{btTransform} de Bullet y un
puntero a \texttt{SceneNode} de Ogre. Es dentro del método
\texttt{setWorldTransform()}(ver listado~\ref{list:motion-state}) donde se realiza la
actualización del nodo de Ogre3D. Bullet invoca de forma automática el
método
\texttt{setWorldTransform()} de cada uno de los cuerpos físicos que forman el mundo.


Dicho método recibe un objeto de tipo \texttt{btTransform}. La clase
btTransform encapsula un cuaternio, que almacena la rotación del
cuerpo físico, y un vector, que almacena su posición. Dado que la
clase \texttt{MotionState} almacena un puntero a un nodo de escena de
Ogre3D, en el momento en que Bullet invoque el
método \texttt{setWorldTransform()}, se actualizará la rotación y
posición de dicho nodo de escena; es decir, se actualizará la posición
y rotación del cuerpo gráfico a partir de la nueva información
recibida.

\begin{listing}[
  language = C++,
  caption = {Creación de un cuerpo rígido},
  label = list:rigid-body-creation]

  btVector3 inertia(0 ,0 ,0);
  if(mass != 0)
    shape->calculateLocalInertia(mass, inertia);

  int radius = 1;
  MotionState* motionState = new MotionState(worldTransform, node);
  btRigidBody::btRigidBodyConstructionInfo
    rigidBodyCI(mass, motionState, new btSphereShape(radius), inertia);

  btRigidBody* rigidBody = new btRigidBody(rigidBodyCI);
  dynamics_world_->addRigidBody(rigidBody);
\end{listing}

Por último, en el listado~\ref{list:rigid-body-creation} se muestra cómo se crea
un cuerpo rígido con una forma física esférica. Es importante saber que Bullet
interpreta que un cuerpo con una masa de 0 kg es un cuerpo que no interacciona
con ninguna fuerza; es decir, permanecerá inamovible en su posición inicial. Por
otra parte, las medidas de las formas físicas de colisión de Bullet están
expresadas en metros desde el centro de la forma física. Esto quiere decir que
la esfera que se crea en el listado~\ref{list:rigid-body-creation} es una esfera
de un metro de radio.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.8\textwidth]{simulacion-esferas.png}
  \caption{Simulación del comportamiento de cuerpos esféricos}
  \label{fig:simulacion-esferas}
\end{figure}

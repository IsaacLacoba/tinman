#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-
path=$(pwd)

for i in $(seq 18 1 28)
do
   mv $path/$i.xcf $path/`expr $i - 1`.xcf
done

#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-
path=$(pwd)

for i in $(seq 13 +1 26)
do
   mv $path/$i.png $path/`expr $i - 1`.png
done

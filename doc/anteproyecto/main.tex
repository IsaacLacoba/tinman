\documentclass{pre-tfg} \usepackage{changepage}

\title{Desarrollo e instrumentación de un videojuego 3D con fines docentes}

\author{Isaac Lacoba Molina}
\advisorFirst{David Villa Alises}
\advisorSecond{}
\advisorDepartment{Tecnologías y Sistemas de Información}
\intensification{Ingeniería de Computadores}
\docdate{2014}{junio}

\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Introducción}

La importancia de los videojuegos en la sociedad actual está fuera de
toda duda. Tanto es así, que un estudio publicado por la
empresa Newzoo~\cite{website:newzoo} afirma que el mercado mundial
del videojuego alcanzará en 2\,017 una cifra de facturación de 102\,900
millones de dólares. Aunque se trata de una estimación basada en la
tendencia actual del mercado mundial, es suficiente para demostrar la
enorme importancia que tienen los videojuegos en la economía mundial.

Desde el punto de vista del desarrollo, los proyectos de este tipo
están caracterizados por involucrar una gran cantidad de disciplinas:
motor del juego, físicas, interfaces de usuario, inteligencia
artificial, networking, programación
gráfica, programación de niveles, diseño
de las mecánicas de juego (gameplay), etc. todo esto sin contar los
roles no relacionados con la programación, como los artísticos o
comerciales.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{roles.png}
  \label{fig:roles}
  \caption[Visión conceptual de un equipo de desarrollo de
    videojuegos]{Visión conceptual de un equipo de desarrollo de
    videojuegos. \url{http://www.cedv.es/}}
\end{figure}

Es sencillo comprender que el desarrollo de videojuegos supone a los desarrolladores
dominar una amplia variedad de disciplinas. Incluso limitándonos a las disciplinas más
técnicas, su dominio requiere años de experiencia y un gran nivel de especialización. En
las grandes compañías de desarrollo existen grupos de trabajo con objetivos asombrosamente
concretos (como por ejemplo, la iluminación del cabello de los personajes humanos),
mientras que en las pequeñas compañías los desarrolladores deben aunar conocimientos de
diversas técnicas.

Además, si atendemos a las demandas del mercado, es común que los
nuevos títulos incluyan algún modo multijugador. Los modos
multijugador ofrecen un gran abanico de posibilidades, ya que permiten
la interacción de varios jugadores de forma simultánea, lo que aporta
un componente social a la experiencia de juego. Esto significa que los
desarrolladores deben diseñar e implementar el modelo de red del
videojuego, con toda la complejidad extra que ello conlleva.


\subsection{Juegos en red}

Utilizar la red para comunicar las diferentes instancias del juego
implica una limitación física insalvable: la latencia. Esto es
especialmente grave cuando esa red es Internet, ya que involucra
computadores que pueden estar repartidos en cualquier parte del
planeta. Eso significa que la latencia será muy diferente entre dos
jugadores cualesquiera debido a que la calidad y características concretas
de su conexión a la red pueden ser muy diferentes.

En cualquier juego en red, existen dos propiedades clave relacionadas con la latencia: la
consistencia y la inmediatez~\cite{smed2006algorithms}.

La consistencia es un indicador de las diferencias entre la percepción que cada jugador
tiene del \emph{mundo} del juego. Si un jugador recibe un mensaje de actualización en un
instante diferente a la de otro jugador, obtendrán una representación diferente del
entorno durante ese intervalo. De modo que, a mayor latencia menos consistencia. Por
ejemplo, en un juego de carreras, es común que la posición dentro del circuito de un
determinado coche no corresponda exactamente para el jugador que lo pilota que para los
contrincantes. Esto puede tener implicaciones negativas sobre la jugabilidad.

Por otro lado, la inmediatez mide el tiempo que le lleva al juego representar las acciones
de los usuarios. Por ejemplo, en un juego FPS (First Person Shooter), el jugador espera
oír inmediatamente el sonido del disparo de su propia arma. Si el juego necesita confirmar
que la acción del jugador y sus consecuencia, tendrá que esperar una validación (remota)
y, por tanto, el resultado puede perder mucho realismo. Como esa comprobación implica
comunicación con otras instancias del juego, una mayor latencia reduce la inmediatez.

Además, aunque es lo deseable, no es posible maximizar a la vez consistencia e
inmediatez. Maximizar la consistencia implica comprobar todas las
acciones y eso reduce la inmediatez. Al mismo tiempo, aumentar la
inmediatez implica crear potenciales inconsistencias. En función del
tipo de juego, será más adecuado sacrificar una sobre la otra.


\subsubsection{Modelo peer-to-peer}
\label{sec:modelo-peer-peer}

El primer modelo de red utilizado en los juegos multijugador estaba
basado en el modelo peer-to-peer~\cite{game-networking-history}. Este
modelo se caracteriza por no tener ningún elemento central, sino que
cada nodo de la red sirve y solicita información a los demás. Aunque
todavía hoy día es posible encontrar juegos que usan este modelo de
red \footnote{Actualmente el modelo Peer-to-Peer es usado
  principalmente en juegos de estrategia en tiempo real(RTS). Como
  ejemplo representativo, podemos nombrar Starcraft 2}, presenta una
serie de problemas que desaconsejan su uso en la mayoría de juegos
actuales.

En este modelo, cada par ejecuta una instancia del juego y envía el estado de su
\emph{mundo} a todos los demás pares. Para poder renderizar la escena, antes todos los
pares deben recibir los mensajes de los demás. La latencia de todos los jugadores tiene un
impacto determinante en la partida, ya que el jugador con mayor latencia tardará mas en
recibir los mensajes de los demás jugadores y en enviar los suyos, retrasando la ejecución
del juego para todos los participantes; es decir, la latencia de los jugadores tiende a
igualarse a la del jugador más «lejano». Esto afecta negativamente a la inmediatez, lo que
no es aceptable en juegos donde ésta sea una propiedad básica (tipo FPS). Por otro lado,
este modelo tiene dificultades para mantener la consistencia de una partida, ya que
comunicar el estado entre todos los jugadores consume mucho ancho de banda, al requerir
un gran número de mensajes.

\subsubsection{Modelo cliente servidor}

Un modelo de red cliente-servidor se caracteriza por tener una entidad central, llamada
servidor, que se encarga de enviar información a los demás participantes en la partida,
normalmente llamados clientes, que se la solicitan. Este modelo reduce el problema de la
latencia debido a que ahora la latencia para un cliente dado solo depende de su conexión
con el servidor, no viéndose afectado (al menos no directamente) por la latencia de los
demás jugadores.

El primer modelo de red basado en una arquitectura cliente-servidor implementaba el
cliente cuyas únicas funciones eran muestrear los eventos de entrada del
jugador (pulsaciones de teclado/ratón), enviarlos al servidor~\cite{latency-compensation} y
renderizar el \emph{mundo}. El servidor por su parte ejecutaba la única instancia real del
juego, recibía mensajes de los clientes y actualizaba el estado del \emph{mundo} basándose
en los mensajes recibidos. Este método conseguía mejorar la consistencia, ya que el
servidor se encarga de comprobar la \emph{legalidad} de las acciones de cada jugador y de
que estos mantengan actualizado su estado, enviándoles mensajes con el estado actualizado
del juego.

Aún con estos cambios, la latencia seguía suponiendo un problema si se
quería mejorar la inmediatez. En los siguientes años, se consiguió
desarrollar una serie de técnicas~\cite{multiplayer-networking} que
conseguirían reducir y enmascarar la latencia. Estas técnicas se
pueden resumir en: mecanismos de predicción en el lado del cliente,
desarrollados por John Carmack y de compensación de la latencia en el
lado del servidor, desarrollados por Yahn Bernier.

Actualmente, la gran mayoría de juegos en red utilizan un modelo
cliente servidor ya que reduce el problema de la latencia, ayudando a
enmascararla, lo que mejora la inmediatez, al tiempo que reduce las
exigencias de ancho de banda.


\subsubsection{Mecanismos de reducción de la latencia}
\label{sec:mecan-de-pred}

En un modelo cliente-servidor actual típico, el servidor envía al cliente una media de 20
mensajes por segundo~\footnote{ver:~\url{http://goo.gl/r3zBEp}}. Si el cliente renderizase
el mundo únicamente con la información que recibe del servidor, no se alcanzaría un
\emph{framerate} (cantidad de cuadros por segundo) aceptable, haciendo que el vídeo del
juego no fuese fluido.

Para poder alcanzar un framerate aceptable~\footnote{El mínimo ratio de renderizado
  aceptado por la industria es 30 frames por segundo. Atendiendo al párrafo anterior, eso
  supone 30 mensajes por segundo, por lo que el servidor debería enviar al menos esta
  cantidad de mensajes para que los clientes pudiesen renderizar el mundo sin implementar
  ningún tipo de técnica de compensación, todo esto suponiendo que los paquetes no sufren
  perdidas, no se ven afectados por factores como el jittering, altas latencias,
  etcétera.} en esta situación se usa una técnica que se conoce como
\emph{interpolación}~\cite{multiplayer-networking}. La \emph{interpolación} pasa por
retrasar el proceso de renderizado en el cliente una cantidad de tiempo aproximadamente
igual a la latencia, al tiempo que se van almacenando en un buffer los snapshots que envía
el servidor. En el ejemplo anterior, si el servidor envía 20 mensajes por segundo, el
cliente recibiría uno cada 50ms. Al retrasar 50ms el renderizado de la escena, el estado
de los objetos del juego podrá ser interpolado tomando el rango de valores comprendido
entre el último mensaje recibido y el previo a ese. Este proceso requiere seguir
recibiendo mensajes del servidor, no siendo posible llevarse a cabo si se pierden mas de
dos mensajes consecutivos, debido a que el cliente se quedaría sin snapshots en el buffer.

Sin embargo, en lugar de esperar a que el servidor envíe un mensaje,
el cliente puede optar por predecir el resultado de sus propios
comandos de usuario, permitiéndole ejecutar las acciones asociadas
inmediatamente. Este proceso sólo es aplicable a las entidades del
mundo que se vean directamente afectada por los comandos del
usuario. Esto es así debido a que se necesita la información de los
demás usuarios para poder saber qué acciones tomaron. Si el runtime del cliente
comete un error al predecir sus propias acciones (por
ejemplo, porque ocurrió una interacción con otro jugador que el
cliente no pudo tener en cuenta durante la predicción) deberá
rectificar en base a los mensajes procedentes del
servidor. Para poder realizar estas correcciones, el cliente guarda
todos las acciones no confirmadas por el servidor en un buffer, de
modo que cuando el servidor le informa de su error, el cliente puede
«rebobinar» sus acciones pasadas y corregir su error.

El servidor por su parte realiza compresión de datos y compensación de
la latencia. La compresión de datos se realiza enviando snapshots, es
decir, resúmenes del estado del juego en un determinado momento, en
lugar de enviar el estado con cada cambio que se produzca. La
compensación de latencia consiste en tener en cuenta la latencia del
cliente a la hora de procesar los mensajes. Por ejemplo, si el cliente
tiene 150ms de latencia, los mensajes que reciba el servidor tendrían
sentido con el estado que el tuviese el \emph{mundo} hace 150ms, de
modo que el servidor «rebobina» el estado del juego para este
jugador a 150ms antes de recibir un mensaje de dicho cliente, de modo
que pueda ejecutar los comandos de usuario en el contexto en el que el
cliente estaba inmerso cuando los ejecutó~\cite{lag-compensation}.

\subsection{Instrumentación}

A raíz de lo dicho anteriormente, podemos deducir que el desarrollo de
videojuegos es una labor compleja, no sólo por la cantidad de las
disciplinas que engloba, sino también por la complejidad de las mismas.

Debido a esta complejidad inherente, monitorizar y entender el
funcionamiento e interacciones de los diferentes componentes del juego
implica implementar una serie de mecanismos adicionales que nos
permitan ver información relevante. Esto podemos hacerlo mediante
varias aproximaciones. Una puede ser la incorporación de un sistema de
logs sobre los que volcar información de inicialización, eventos
sucedidos durante la ejecución, etcétera. Otra aproximación pasa por
ofrecer un acceso alternativo al estado de los objetos; es decir,
mediante introspección. Estos son dos ejemplos de las muchas opciones
existentes.

La implementación de estos y otros mecanismos de instrumentación es interesante y valiosa
para el desarrollador desde dos puntos de vista. Por un lado, estos mecanismos le
facilitan la labor de crear pruebas, tanto unitarias, de integración y de
comportamiento. Las pruebas permiten la detección temprana de errores en nuestro
proyecto. Por otro lado, al exponer detalles del funcionamiento del videojuego y de las
decisiones que se están tomando en cada punto, es más sencillo para un desarrollador
entender qué está ocurriendo y porqué.


\subsection{Género del juego}

Como caso concreto para llevar a la práctica todas estas nociones,
tanto de instrumentación como de juego en red, se ha elegido un juego
de carreras de coches. Este tipo de juego permite incorporar
complejidad de forma incremental al mismo tiempo que ofrece un
prototipo funcional en todas sus fases. Es posible comenzar con una
versión monojugador con una interacción simple con el entorno, y en
sucesivas iteraciones incorporar físicas, IA, multi-jugadores, etc.

Además, un juego de coches resulta adecuado para plantear la
instrumentación y es sencillo proporcionar información gráfica
adicional sobre el funcionamiento interno del juego y de la
algorítmica relacionada con el juego en red.


\section{Tecnología específica}

\begin{table}[!h] \centering
  \caption{Tecnología Específica cursada por el alumno}
  \label{tab:tec-especifica}

  \zebrarows{1}
  \begin{tabular}{p{0.6\textwidth}r} \textbf{Marcar la tecnología
cursada} \\ \hline Tecnologías de la Información & \\ Computación & \\
Ingeniería del Software & \\ Ingeniería de Computadores & X \\ \hline
  \end{tabular}
\end{table}

El cuadro~\ref{tab:competencias} describe las competencias de dicha
intensificación que se desarrollan en esta propuesta de proyecto.

{\small
\begin{table}[p] \centering
  \caption{Justificación de las competencias específicas abordadas en el TFG}
  \label{tab:competencias}

  \begin{adjustwidth}{-1.5cm}{-0.6cm} \zebrarows{1}
    \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
      (3) Capacidad de analizar y evaluar arquitecturas de computadores, incluyendo plataformas
      paralelas y distribuidas, así como desarrollar y optimizar software para las mismas;
      (4) Capacidad de diseñar e implementar software de sistema y de comunicaciones.
      &
        Uno de los objetivos principales del proyecto es desarrollar una infraestructura de
        comunicaciones cliente/servidor que contemple las graves restricciones con las que
        se enfrenta un videojuego: compensación de latencia y control de la sobrecarga entre
        servidor y clientes. En concreto, el sistema de comunicaciones estará basado en un
        middleware de comunicaciones orientado a objetos.
   \end{tabular}
 \end{adjustwidth}

 \begin{adjustwidth}{-1.5cm}{-0.6cm} \zebrarows{1}
   \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
     Capacidad de analizar, evaluar y seleccionar las plataformas hardware y software más
     adecuadas para el soporte de aplicaciones empotradas y de tiempo real.
     &
       El modo de juego en red impone restricciones de tiempo que deben ser consideradas en
       ambos extremos. Todos los eventos de red incorporan una marca de tiempo y validan
       que pertenece a la ventana de tiempo que se evalúa en cada instante. Por tanto, se
       trata en cierto sentido de tiempo real blando.
   \end{tabular}
 \end{adjustwidth}

 \begin{adjustwidth}{-1.5cm}{-0.6cm} \zebrarows{1}
   \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
     Capacidad para comprender, aplicar y gestionar la garantía y seguridad de los sistemas
     informáticos.
     &
       Desde el punto de vista de la seguridad, un videojuego con soporte de red debe
       considerar al menos integridad de las comunicaciones. Si un jugador consigue
       sintetizar mensajes que sean validados por el servidor, podría conseguir una
       ventaja significativa en el objetivo del juego (se conoce coloquialmente por su
       nombre en inglés: \emph{cheats}). El sistema debe detectar e impedir este tipo de
       mensajes fraudulentos.
   \end{tabular}
 \end{adjustwidth}

 \begin{adjustwidth}{-1.5cm}{-0.6cm} \zebrarows{1}
   \begin{tabular}{p{0.5\linewidth}p{0.5\linewidth}}
     Capacidad para diseñar, desplegar, administrar y gestionar redes de computadores.
     &
       Aunque el objeto del proyecto no implica diseño o administración de redes, sí
       requiere del autor de los conocimientos necesarios para entender la escala a la que
       debe trabajar, principalmente en términos de latencia y ancho de banda, tanto en la
       LAN como en la WAN. Cuándo varios usuarios dispersos en internet
       participan en la misma partida, este tipo de consideraciones son cruciales para
       conseguir un equilibrio adecuado entre consistencia o inmediatez, y de no ser
       posible, evitar que un jugador excesivamente lento participe en la partida.
   \end{tabular}
 \end{adjustwidth}
\end{table}
}

\section{Objetivos}

En primer lugar se pretende desarrollar un videojuego, utilizando
gráficos en 3D. Aunque el videojuego en sí ya es un objetivo de
envergadura, este proyecto pretende utilizarlo como base para dos
objetivos adicionales:
%
\begin{itemize}
\item Diseño e implementación de mecanismos de instrumentación del juego que permita
  aplicar técnica de testing automático y al mismo tiempo exponer los mecanismos con fines
  didácticos y de depuración.
\item Diseño e implementación de un modelo de red para una modalidad multijugador, que
  aprovechará los citados mecanismos de instrumentación con los mismos fines.
\end{itemize}

Otro objetivo no menos importante es crear un ejemplo completo de videojuego que pueda
servir a futuros programadores a aprender las técnicas básicas y los fundamentos
técnicos esenciales del desarrollo de un videojuego. Por ese motivo se hará énfasis en los
siguientes aspectos:
%
\begin{itemize}
\item Claridad del código.
\item Uso de patrones de diseño~\cite{Gamma:1995:DPE:186897}.
\item Técnicas de introspección de objetos.
\item Acceso al código fuente y propio proyecto. Por esta razón se liberará bajo una
  licencia libre.
\end{itemize}

\section{Métodos y fases de trabajo}

Para el desarrollo del proyecto, seguiremos una metodología iterativa e incremental,
guiada por una serie de hitos claramente definidos.  En la medida de lo posible se
dirigirá el desarrollo del videojuego mediante pruebas. Hay que tener en cuenta que las
herramientas y las metodologías de desarrollo actuales en este campo están en fases
tempranas. De hecho, este proyecto trata precisamente de desarrollar este tipo de soporte
gracias a los mecanismos de instrumentación. La lista de hitos o tareas son:
%
\begin{itemize}
\item Desarrollar un videojuego de carreras completamente funcional.
\item Versión inicial con un control del coche mediante teclado.
\item Físicas básicas para las colisiones, aceleración y frenado.
\item Interfaces para creación de partidas y sistema de puntuación.
\item Incorporación de un sistema de \emph{logging} que permita registrar los eventos más
  relevantes.
\item Implementación del soporte básico de instrumentación.
\item Implementar un modelo de red cliente/servidor.
\item Implementar mecanismos de compensación de latencia en el lado del servidor.
\item Implementar mecanismos de interpolación y extrapolación en el lado
  del cliente.
\end{itemize}


\subsection{Metodología}

El desarrollo de videojuegos es una labor muy susceptible a
errores. Tener pruebas que ayuden a ver que el código que vamos
generando no produce conflictos con el previamente existente es clave
a la hora de desarrollar un producto de mayor calidad.

TDD (Test-Driven Develpment) es una metodología de desarrollo software
creada por Kent Beck. Se fundamenta en guiar la creación del software
a partir de las pruebas, típicamente unitarias.

La esencia de esta metodología~\cite{beck-test-drivendevelopment-2003}
radica en escribir las pruebas antes que el código que realiza la
funcionalidad que se quiere probar. Dicha prueba debe fallar en primer
lugar, ya que al escribir antes la prueba que el código que se quiere
probar, no puede existir código que realice dicha funcionalidad. Tras
escribir la prueba, se debe escribir el código mínimo que haga que la
prueba pase favorablemente. Por último, se pasa a la fase de
refactorización, consistente en eliminar o reescribir el código
redundante producido durante el proceso anterior. Se trata de una fase
de limpieza de código muy importante, ya que aumenta la calidad del
código y la mantenibilidad del mismo.

El uso de esta metodología nos asegura, en primer lugar, que el código
que se va generando no hace fallar al escrito anteriormente. En
segundo lugar, las propias pruebas sirven como documento de validación
de requisitos, ya que se escriben teniendo en cuenta estos últimos. En
último lugar, se evita la implementación de funcionalidad que no esté
presente en los requisitos del proyecto, al centrar el desarrollo en
el código estrictamente necesario para hacer que pasen las pruebas.

\section{Medios que se utilizarán}

A continuación, procederemos a listar las herramientas software y
hardware de que haremos uso.
\subsection{Recursos software}

Para el desarrollo de este proyecto se hará uso de:
\begin{itemize}
\item Como sistema operativo se usará Debian, una distribución de GNU/Linux.
\item El lenguaje de programación utilizado será
  C++~\cite{Stroustrup:2013:CPL:518791} y como compilador usaremos
  g++.
\item En cuanto al motor de renderizado usaremos la librería
  Ogre3D~\cite{Junker:2006:POP:1177262}.
\item Como motor de físicas usaremos Bullet~\cite{bullet-physics}.
\item Para desarrollar los menus, usaremos la librería CEGUI~\cite{cegui}
\item Framework de pruebas: Bandit~\cite{bandit}
\item Midleware de comunicaciones: ZeroC Ice~\cite{zeroc}
\item Sistema de control de versiones: git~\cite{git}
\end{itemize}
\subsection{Recursos hardware}
Se hará uso de un ordenador Dell XPS 420 y de un Mountain modelo
ivy-11.


{ \small \bibliographystyle{es-alpha}
\bibliography{anteproyecto} }

\end{document}

% Local Variables: % fill-column: 90 % End:

%% Local Variables: %% coding: utf-8 %% mode: flyspell %%
ispell-local-dictionary: "castellano8" %% End:

%  LocalWords:  multijugador jugabilidad Doom buffer renderizar

#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-
wget https://bitbucket.org/cegui/cegui/get/v0-8.zip
unzip v0-8.zip;
cd cegui-cegui-*;
mkdir build;
cd build;

# Several optinal dependencies
sudo apt-get -y install cmake python-opengl pyside-tools libboost-python-dev libghc-glfw-dev libglew-dev libglm-dev;
cmake ../;
sudo make -j4 install;

# update dynamic library cache
sudo ldconfig

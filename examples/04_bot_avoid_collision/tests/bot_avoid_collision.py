#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-
import unittest
import time
import sys
import hamcrest
from commodity.os_ import SubProcess
from commodity.testing import wait_that, call_with
from commodity.net import localhost, listen_port

import Ice
Ice.loadSlice('../../src/network/tinman.ice -I/usr/share/Ice-3.5.1/slice')
import Tinman

class RemoteGame(object):
    def __init__(self):
        self.cars = []
        self.broker = Ice.initialize()

    def add_car(self, proxy):
        proxy = self.broker.stringToProxy(proxy)
        self.cars.append(Tinman.InstrumentatedCarPrx.checkedCast(proxy))

    def get_car(self, index):
        return self.cars[index]

class TestBotAvoidCollisions(unittest.TestCase):
    def setUp(self):
        server = SubProcess("./main --bots 2 --track config/circuit.data --frames 1000", stdout=sys.stdout)
        server.start()
        wait_that(localhost, listen_port(10000))
        self.game = RemoteGame()

        self.game.add_car("Bot0: tcp -h 127.0.0.1 -p 10000")
        self.game.add_car("Bot1: tcp -h 127.0.0.1 -p 10000")

        self.car0 = self.game.get_car(0)
        self.car1 = self.game.get_car(1)

        self.car0.move(Tinman.Vector3(-2, 0, -44))
        self.car1.move(Tinman.Vector3(3, 0, -42))

        self.addCleanup(server.terminate)

    def test_avoid(self):
        self.assertFalse(self.car1.isStuck())
        self.assertFalse(self.car0.isStuck())

        time.sleep(2.5)

        # wait_that(self.car0.isStuck, call_with().returns(True))
        # wait_that(self.car1.isStuck, call_with().returns(True))

        self.assertFalse(self.car0.isColliding())
        self.assertFalse(self.car1.isColliding())

#include <Ice/BuiltinSequences.ice>

module Tinman {
  enum Action {AccelerateBegin, AccelerateEnd,
               BrakeBegin, BrakeEnd,
               TurnRight, TurnLeft, TurnEnd,
               UseNitro};

  struct Quaternion {
    float x;
    float y;
    float z;
    float w;
  };

  struct Vector3 {
    float x;
    float y;
    float z;
  };


  interface InstrumentatedCar {
    void exec(Tinman::Action action);

    int getNitro();
    int getLap();
    Vector3 getPosition();
    Vector3 getVelocity();
    Quaternion getOrientation();
    Vector3 getDirection(int factor);
    float getSpeed();
    bool isColliding();
  };
};

#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-
import unittest
import time
import sys
import hamcrest
from commodity.os_ import SubProcess
from commodity.testing import wait_that, call_with
from commodity.net import localhost, listen_port

import Ice
Ice.loadSlice('../../src/network/tinman.ice -I/usr/share/Ice-3.5.1/slice')
import Tinman

class RemoteGame(object):
    def __init__(self):
        self.cars = []
        self.broker = Ice.initialize()

    def add_car(self, proxy):
        proxy = self.broker.stringToProxy(proxy)
        self.cars.append(Tinman.InstrumentatedCarPrx.checkedCast(proxy))

    def get_car(self, index):
        return self.cars[index]


class TestBotDontGetStuck(unittest.TestCase):
    def setUp(self):
        server = SubProcess("./main --bots 1 --track config/circuit.data --frames 6000", stdout=sys.stdout)
        server.start()
        wait_that(localhost, listen_port(10000))
        self.game = RemoteGame()
        self.game.add_car("Bot0: tcp -h 127.0.0.1 -p 10000")

        self.addCleanup(server.terminate)

    def test_right_1(self):
        self.car0 = self.game.get_car(0)
        self.car0.move(Tinman.Vector3(-1, 2, -40))

        self.assertFalse(self.car0.isStuck())
        time.sleep(2.5)

        self.assertFalse(self.car0.isStuck())

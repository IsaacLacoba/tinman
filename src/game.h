// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef GAME_H
#define GAME_H
#include <memory>
#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>

#include "physicsdebugdrawer.h"
#include "playerfactory.h"
#include "carfactory.h"

#include "initial.h"
#include "menu.h"
#include "pause.h"
#include "play.h"
#include "results.h"

class StateMachine;

enum class StateName { Initial, Menu, Play, Pause, Results};
class Game: public std::enable_shared_from_this<Game> {
  const float FPS = 60;
  Timer timer_;
  std::shared_ptr<StateMachine> state_machine_;
  int frame_counter_;
  PhysicsDebugDrawer::shared physics_drawer_;

 public:
  typedef std::shared_ptr<Game> shared;
  enum class Mode {Bots, Network};

  Mode mode_;
  float delta_;
  float finish_time_;
  std::vector<std::pair<std::string, int>> race_results_;
  bool race_started_;
  std::string ip_, nick_;
  int port_;
  bool debug_;

  Scene::shared scene_;
  EventListener::shared input_;
  GUI::shared gui_;
  Sound::shared sound_;
  Physics::shared physics_;
  Animation::shared animation_;
  CarFactory::shared car_factory_;
  Session::shared session_;

  Ice::CommunicatorPtr communicator_;
  ObjectAdapterPtr debug_adapter_;

  Game();
  Game(Session::shared session);
  virtual ~Game();

  void start();
  void start(int seconds);
  void shutdown();
  bool gui_shutdown(const CEGUI::EventArgs &event);

  Ice::AsyncResultPtr login();
  void complete_login(Ice::AsyncResultPtr async_result);
  void disconnect();

  void create_remote_players(Tinman::LoginInfo login_info);
  void update_remote_players(Tinman::Snapshot snapshot);

  void new_session();

  void add_race(Race::shared race);
  void add_debug_drawer();
  void add_debug_adapter();

  void set_current_state(StateName state);
  void change_state();

  Player::shared get_human_player();
 private:
  void init();
  void realize();

  void step();
  void game_loop();
  void game_loop(int seconds);
};
#endif

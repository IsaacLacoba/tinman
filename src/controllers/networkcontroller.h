#ifndef NETWORKCONTROLLER_H
#define NETWORKCONTROLLER_H
#include "controller.h"

class NetworkController: public Controller {
 public:
  NetworkController(Car::shared car);
  virtual ~NetworkController();

  void configure();
  void update(float delta);
  void exec(Tinman::Action action);

  int get_snapshots_per_frame(float delta);

 private:
  float duration_, last_size_;
  int last_num_sequence_;
};

#endif

// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "carcontroller.h"

CarController::CarController() {
  load_parameters("config/car.config");

  accelerating_ = turning_ = false;
  f_engine_ = f_braking_ = steering_ = 0.f;
  nitro_ = 10;
  inverted_ = false;
}

CarController::~CarController() {

}

void
CarController::load_parameters(std::string file) {
  Parser parser;
  std::string pattern = R"(\w*\s*:\s*(-?\b\d*.\d*))";
  parser.inject(pattern);

  parser.open(file);
  Parser::Results parameters = parser.get_results();
  int index = 0;

  f_max_engine_               = str_to_float(parameters[index++][1]);
  acceleration_               = str_to_float(parameters[index++][1]);
  deceleration_               = str_to_float(parameters[index++][1]);
  f_max_braking_              = str_to_float(parameters[index++][1]);
  steering_increment_         = str_to_float(parameters[index++][1]);
  steering_clamp_             = str_to_float(parameters[index++][1]);
  mass_                       = str_to_float(parameters[index++][1]);
  gvehicle_steering_          = str_to_float(parameters[index++][1]);
  wheel_radius_               = str_to_float(parameters[index++][1]);
  wheel_width_                = str_to_float(parameters[index++][1]);
  suspension_stiffness_       = str_to_float(parameters[index++][1]);
  wheel_friction_             = str_to_float(parameters[index++][1]);
  suspension_damping_         = str_to_float(parameters[index++][1]);
  suspension_compression_     = str_to_float(parameters[index++][1]);
  roll_influence_             = str_to_float(parameters[index++][1]);
  connection_height_          = str_to_float(parameters[index++][1]);
}

void
CarController::realize( std::vector<Ogre::SceneNode*> wheels_nodes) {
  wheels_nodes_ = wheels_nodes;
}

void
CarController::accelerate() {
  if(f_engine_ >=  f_max_engine_) {
    accelerating_ = false;
    return;
  }
  f_engine_ += acceleration_;
  accelerating_ = true;
}

void
CarController::stop_accelerating() {
  accelerating_ = false;
  f_braking_ = deceleration_;
}


void
CarController::brake() {
  accelerating_ = false;
  braking_ = true;
  f_braking_ = f_max_braking_;
  f_engine_ = (f_engine_ <= f_max_engine_) ? -f_max_engine_: f_engine_ - deceleration_;
}

void
CarController::stop_braking() {
  braking_ = false;
  f_braking_ = deceleration_;
}

void
CarController::turn(Direction direction) {
  if(inverted_)
    direction = direction == Direction::right? Direction::left : Direction::right;

  turn_wheels(direction);

  if(direction == Direction::right)
    steering_ = (steering_ < -steering_clamp_)?
      -steering_clamp_ : steering_ - steering_increment_;
  else
    steering_ = (steering_ > steering_clamp_)?
      steering_clamp_ : steering_ + steering_increment_;
}

void
CarController::turn_wheels(Direction direction) {
  Ogre::Degree rotation_angle = (direction == Direction::right)
    ? Ogre::Degree(150) : Ogre::Degree(30);

  wheels_nodes_[0]->setOrientation(Ogre::Quaternion(rotation_angle,
					    Ogre::Vector3(0, 1, 0)));
  wheels_nodes_[1]->setOrientation(Ogre::Quaternion(rotation_angle,
					    Ogre::Vector3(0, 1, 0)));
}

void
CarController::reset_wheels() {
  wheels_nodes_[0]->setOrientation(Ogre::Quaternion(Ogre::Degree(0),
					    Ogre::Vector3(0, 1, 0)));
  wheels_nodes_[1]->setOrientation(Ogre::Quaternion(Ogre::Degree(0),
					    Ogre::Vector3(0, 1, 0)));
}

void
CarController::stop_turning() {
  steering_ = 0;
  reset_wheels();
}

void
CarController::control_speed() {
    f_engine_ = (f_engine_ - f_braking_ <= 0) ? 0 : f_engine_ - deceleration_;
}

void
CarController::use_nitro() {
  if(nitro_ <= 0)
    return;
  nitro_--;
  f_engine_ = 3 * f_max_engine_;
}

void
CarController::update(float delta) {
  control_speed();
}

float
CarController::str_to_float(std::string string) {
  std::istringstream ss(string);
  float number;
  ss >> number;
  return number;
}

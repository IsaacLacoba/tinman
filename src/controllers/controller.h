#ifndef CONTROLLER_H
#define CONTROLLER_H
#include <map>
#include <memory>
#include <queue>

#include "tinman.h"
#include "network.h"

class Controller {
 public:
  std::queue<Tinman::CarInfo> updates_;
  Car::shared car_;
  ControllerObserver::shared observer_;

  typedef std::shared_ptr<Controller> shared;

  Controller(Car::shared car);
  virtual ~Controller();

  virtual void configure() = 0;
  virtual void update(float delta) = 0;
  virtual void exec(Tinman::Action action) = 0;

  void store(Tinman::CarInfo car_info);

  void disconnect();
};

#endif

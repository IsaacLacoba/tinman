// -*- coding: utf-8; mode: c++; tab-width: 4 -*-
#include "bot.h"
#include "race.h"

BotController::BotController(Physics::shared physics,
                             Car::shared car):  Controller(car) {
  index_ = 0;
  stuck_ = collide_ = false;
  stucked_ = colliding_ = 0.f;
  physics_ = physics;
  current_segment_ = previous_segment_ = Section {};
}

BotController::~BotController() {
}

void
BotController::add_race(Race::shared race) {
  index_ = 0;
  stucked_ = colliding_ = 0.f;
  race_ = race;
}

void
BotController::configure() {
}

void
BotController::update(float delta) {
  if(!detect_obstacles(delta))
    go_to_waypoint();

  car_->update(delta);
}

bool
BotController::detect_obstacles(float delta) {
  bool obstacle_detected = false;
  // FIXME: refactor!!! smelly code
  // if (car_->is_colliding() && !collide_) {
  //   collide_ = true;
  // }

  // if (collide_) {
  //   std::cout << "[BotController] is colliding: " << colliding_ << std::endl;
  //   colliding_ += delta;
  //   if(colliding_ > collide_limit_) {
  //     colliding_ = 0.f;
  //     collide_ = false;
  //     car_->colliding_ = false;
  //   }

  //   exec(Tinman::AccelerateEnd);
  //   exec(Tinman::BrakeBegin);

  //   obstacle_detected = true;
  //   return obstacle_detected;
  // }

  if (car_->is_stuck() && !stuck_) {
	std::cout << "[BotController] is_stuck" << std::endl;
  	stuck_ = true;
  	car_->invert_direction();
  }

  if (stuck_) {
  	std::cout << "[BotController] is stuccked" << std::endl;
  	stucked_ += delta;
  	if (stucked_ > stuck_limit_) {
  	  stucked_ = 0.f;
  	  stuck_ = false;
	  car_->stuck_ = false;
  	  car_->invert_direction();
  	}

  	exec(Tinman::AccelerateEnd);
    exec(Tinman::BrakeBegin);

    obstacle_detected = true;
    return obstacle_detected;
  }

  return obstacle_detected;
}

void
BotController::go_to_waypoint() {
  current_segment_ = car_->current_segment_;
  auto previous_segment_ = race_->get_section(current_segment_.id - 1);
  std::cout << "[BotController] " << __func__ << std::endl;
  exec(Tinman::AccelerateBegin);
  if (current_segment_.type == Section::Meta ||
      current_segment_.type == Section::Rect1 ||
      current_segment_.type == Section::Rect2) {
    exec(Tinman::TurnEnd);
    return;
  }

  if (current_segment_.type == Section::RightCurve1) {
    if (previous_segment_.type == Section::Rect1 ||
        previous_segment_.type == Section::Meta ||
        previous_segment_.type == Section::LeftCurve2) {
      exec(Tinman::TurnRight);
      return;
    }

    if (previous_segment_.type == Section::Rect2) {
      exec(Tinman::TurnLeft);
      return;
    }
  }

  if (current_segment_.type == Section::RightCurve2) {
    if (previous_segment_.type == Section::RightCurve1 ||
        previous_segment_.type == Section::Rect2) {
      exec(Tinman::TurnRight);
      return;
    }

    if (previous_segment_.type == Section::Rect1) {
      exec(Tinman::TurnLeft);
      return;
    }
  }

  if (current_segment_.type == Section::LeftCurve1) {
    if (previous_segment_.type == Section::Rect2) {
      exec(Tinman::TurnRight);
      return;
    }

    if (previous_segment_.type == Section::Rect1 ||
        previous_segment_.type == Section::RightCurve2) {
      exec(Tinman::TurnLeft);
      return;
    }
  }


  if (current_segment_.type == Section::LeftCurve2) {
    if(previous_segment_.type == Section::Rect1) {
      exec(Tinman::TurnRight);
      return;
    }

    if (previous_segment_.type == Section::Rect2 ||
        previous_segment_.type == Section::LeftCurve1) {
      exec(Tinman::TurnLeft);
      return;
    }
  }
}

void
BotController::exec(Tinman::Action action) {
  car_->exec(action);
}

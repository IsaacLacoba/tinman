// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "human.h"

HumanController::HumanController(EventListener::shared listener,
                        KeyBinding key_bindings, Car::shared car): Controller(car) {
  input_ = listener;
  key_bindings_ = key_bindings;
  network_mode_ = true;
}

HumanController::~HumanController() {
}

void
HumanController::configure() {
  std::cout << "[HumanController] configure" << std::endl;
  for(auto key_bind: key_bindings_) {
    input_->add_hook({key_bind.first, EventTrigger::OnKeyPressed}, EventType::repeat,
                     std::bind((void(HumanController::*)
                     (Tinman::Action))(&HumanController::exec), this, key_bind.second.first ));
    input_->add_hook({key_bind.first, EventTrigger::OnKeyReleased}, EventType::doItOnce,
                     std::bind((void(HumanController::*)
                     (Tinman::Action))(&HumanController::exec), this, key_bind.second.second));
  }
}

void
HumanController::update(float delta) {
  // std::cout << "car speed: "<< car_->get_speed() << std::endl;
  // std::cout << "id: "<< car_->current_segment_.id << std::endl;
  car_->update(delta);
}

void
HumanController::exec(Tinman::Action action) {
  car_->exec(action);
  if(network_mode_)
    observer_->notify(action);
}

#ifndef PLAYER_H
#define PLAYER_H
#include "bot.h"
#include "human.h"
#include "networkcontroller.h"
#include "icontroller.h"

class Session;

class Player {
  Scene::shared scene_;
  Sound::shared sound_;
  Animation::shared animation_;

 public:
  typedef std::shared_ptr<Player> shared;
  std::string race_ended_;
  Controller::shared controller_;
  int money_;
  std::string nick_;

  Player(std::string nick, Controller::shared controller,
	 Sound::shared sound, Animation::shared animation);
  virtual ~Player();

  void realize(Physics::shared physics, Scene::shared scene, std::string  name,
          btVector3 position, std::string material, int checkpoints, Ice::Byte id);
  Ice::AsyncResultPtr login(std::shared_ptr<Session> session);
  Ice::AsyncResultPtr next_race();
  Tinman::LoginInfo complete_login(Ice::AsyncResultPtr async_result);

  void update(float delta);
  void disconnect();

  void store(Tinman::CarInfo car_info);
  void purchase();

  Car::shared get_car();
  std::string get_nick();
  void set_nick(std::string nick);
  Ice::Byte get_id();

  void pick_money( int amount, PowerUp::shared money);
  void pick_nitro( PowerUp::shared nitro);

  void reset(int checkpoints, btVector3 position);

  std::pair<std::string, int> get_socket();
  void set_socket(std::pair<std::string, int> socket);

 private:
  void add_animation(std::string node, std::string animation);
  void clean_animation();
};
#endif

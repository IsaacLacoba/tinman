// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#include "race.h"
#include "game.h"

Race::Race(std::string track) {
  circuit_ = std::make_shared<Track>(track);
  number_of_laps_ = 1;
}

Race::~Race() {
}

void
Race::update(float delta) {
  nitro_->update(delta);
  money_->update(delta);
  update_players(delta);

  game_->race_results_ = get_players_positions();
  if(has_ended()) {
    game_->finish_time_ = timer_.get_time_since_start();
    game_->change_state();
  }
}

void
Race::realize(Game::shared game, std::vector<Player::shared> players) {
  std::cout << "[Race] realize" << std::endl;
  game_ = game;
  players_ = players;

  create_powerups();

  circuit_->next(game_->scene_, game_->physics_);
  configure_grid();
  config_cars_in_login_order();
  add_collision_hooks();
}

void
Race::create_powerups() {
  nitro_ = std::make_shared<PowerUp>(game_->scene_, game_->physics_, game_->animation_);
  money_ = std::make_shared<PowerUp>(game_->scene_, game_->physics_, game_->animation_);

  nitro_->realize(PowerUp::Type::Nitro);
  money_->realize(PowerUp::Type::MoneyBag);

  nitro_->active_animation("spining");
  money_->active_animation("spining");
}

void
Race::start() {
  timer_.start();
}

void
Race::config_race() {
  game_->sound_->play("media/sound/background_sound.wav");
  game_->sound_->play("media/sound/acceleration.wav");
}

void
Race::pause() {
  timer_.stop();
}

void
Race::next(std::string data_file) {
  circuit_->next(data_file);
  configure_grid();

  add_collision_hooks();
  reset_cars();
  reset_powerups();
}

void
Race::reset() {
  circuit_->reset();
  starting_grid_.clear();

  timer_.restart();
  pause();
}

void
Race::reset_cars() {
  int index = 0;
  for(auto player: players_)
    player->reset(circuit_->segments_.size(),
                  starting_grid_[index++]);
}

void
Race::reset_powerups() {
  nitro_->reset();
  money_->reset();
}

bool
Race::has_ended() {
  std::vector<Car::shared> cars;
  for(auto player: players_)
    cars.push_back(player->get_car());

  return end(cars);
}

bool
Race::end(std::vector<Car::shared> cars) {
  for(auto car: cars)
    if(car->get_lap() == number_of_laps_)
      return true;

  return false;
}

void
Race::add_collision_hooks() {
  for(auto& player: players_){
    game_->physics_->add_collision_hooks({ nitro_->body_, player->get_car()->chassis_body_},
                                 std::bind(&Player::pick_nitro, player.get(), nitro_));
    game_->physics_->add_collision_hooks({ money_->body_, player->get_car()->chassis_body_},
                                 std::bind(&Player::pick_money, player.get(), 10000, money_));
    add_collision_with_circuit(player->get_car());
    add_collision_with_cars(player);
  }
}

void
Race::add_collision_with_circuit(Car::shared car) {
  for(auto& section: circuit_->segments_) {
    // std::cout << "id: " << section.id
    //           << "tipo :" << section.type << std::endl;
    game_->physics_->add_collision_hooks(Physics::CollisionPair{car->chassis_body_,
          section.body},
      std::bind(&Car::set_race_segment_info, car.get(), section));
  }
}

void
Race::add_collision_with_cars(Player::shared target) {
  for(auto player: players_) {
    if (player->nick_ == target->nick_)
      continue;
    std::cout << "[Race]" << __func__ << "nick: " << target->nick_ << std::endl;

    game_->physics_->add_collision_hooks({target->get_car()->chassis_body_, player->get_car()->chassis_body_},
	 std::bind(&Car::collide_with_car, target->get_car().get()));
    game_->physics_->add_collision_hooks({player->get_car()->chassis_body_, target->get_car()->chassis_body_},
	 std::bind(&Car::collide_with_car, player->get_car().get()));
  }
}


void
Race::update_players(float delta) {
  for(auto player: players_)
    player->update(delta);
}

void
Race::config_cars_in_login_order() {
  std::stringstream material, name;
  int id = 0;
  for(auto player: players_) {

    name << "car" << id + 1;
    if(player->nick_ == "Tinman")
      material << "tinman";
    else
      material << "car";
    game_->physics_->print_vector(std::string{"[Race] config cars in login order"},
                           starting_grid_[id]);
    player->realize(game_->physics_, game_->scene_ , name.str(),
                              starting_grid_[id++],  material.str(),
                              circuit_->segments_.size(), id);
    material.str("");
    name.str("");
  }
}

std::vector<std::pair<std::string, int>>
  Race::get_players_positions() {
  std::vector<std::pair<std::string, int>> players_positions;
  for(auto player: players_) {
    Car::shared car = player->get_car();
    players_positions.push_back({player->get_nick(), car->race_position_});
  }

  return players_positions;
}

Section
Race::get_section(int index) {
  if(index > circuit_->segments_.size())
    return circuit_->segments_.back();
  if(index < 0)
    return circuit_->segments_.front();
  return circuit_->segments_[index];
}

void
Race::configure_grid() {
  int x = circuit_->segments_[0].position.x();
  int z = circuit_->segments_[0].position.z();
  int z_delta = 4;
  for (int i = 0; i <= 4; ++i) {
    if(z_delta > 0)
      x += 7;
    z_delta *= -1;
    z += z_delta;
    starting_grid_.push_back(btVector3(x, 2, z));
  }
}

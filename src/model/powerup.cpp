// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "powerup.h"

PowerUp::PowerUp(Scene::shared scene, Physics::shared physics,
		 Animation::shared animation) {
  std::cout << "[PowerUp] contructor" << std::endl;
  animation_ = animation;
  scene_ = scene;
  physics_ = physics;
  removed_ = false;
}

PowerUp::~PowerUp() {
  delete entity_;
  delete node_;
  delete shape_;
  delete body_;
}

void
PowerUp::realize(PowerUp::Type object) {
  std::cout << "[PowerUp] realize" << std::endl;
  std::string name = (object == PowerUp::Type::Nitro) ?
    "nitro": "bag";
  std::string mesh = (object == PowerUp::Type::Nitro) ?
    "nitro.mesh": "bag.mesh";

  std::stringstream file;
  file << "config/" << name << "_locations.data";
  file_ = file.str();
  read_locations(file_);

  node_ = scene_->create_graphic_element(Ogre::Vector3(last_location_.first.x(),
                                                       last_location_.first.y(),
                                                       last_location_.first.z()), name, mesh);
  entity_ = static_cast<Ogre::Entity*>
    (scene_->get_node(name)->getAttachedObject(0));

  btVector3 size(2, 2, 2);
  shape_ = physics_->create_shape(size);

  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btVector3 origin = last_location_.first;
  btTransform transformation(rotation, origin);
  body_ = physics_->
    create_rigid_body(transformation, node_, shape_, 1);
  body_->setActivationState(DISABLE_DEACTIVATION);
  body_->setRestitution(0.2);

  start_ = 0;
}

void
PowerUp::read_locations(std::string file) {
  Parser parser;
  std::string pattern =
    R"(position:\s*\{(-?\b\d*.\d*), (-?\b\d*.\d*), (-?\b\d*.\d*)\}\s*duration: (\b\d*.\d*))";
  parser.inject(pattern);
  parser.open(file);
  for(auto location: parser.get_results())
    locations_.push_back({Position(std::stol(location[1]),
                                   std::stol(location[2]),
                                   std::stol(location[3])),
          seconds(std::stol(location[4]))});
  last_location_ = locations_.back();
  locations_.pop_back();
}

void
PowerUp::update(float delta) {
  start_ += delta;

  if(last_location_.second > start_)
    return;

  if(locations_.empty())
    return;

  last_location_ = locations_.back();
  locations_.pop_back();
  replace(last_location_.first);
}

void
PowerUp::pick_up() {
  // scene_->destroy_node("powerup_particle" + node_->getName());
  // scene_->get_particle("powerup_particle" + node_->getName(), "PowerUp/PowerUp",
  //                      node_->getPosition());
  remove_from_scene();
  removed_ = true;
}

void
PowerUp::replace(btVector3 new_position) {
  physics_->set_position(body_, new_position);
  if(removed_){
    add_to_scene();
    removed_ = false;
  }
}

void
PowerUp::add_to_scene() {
  physics_->add_rigid_body(body_);
  scene_->add_child(scene_->get_node(""), node_);
}

void
PowerUp::remove_from_scene() {
  physics_->remove_rigid_body(body_);
  scene_->remove_child("", node_);
}

void
PowerUp::reset() {
  start_ = 0;
  locations_.clear();
  read_locations(file_);
  replace(last_location_.first);
}

void
PowerUp::active_animation(std::string animation_name) {
  animation_->activate(entity_, animation_name);
}

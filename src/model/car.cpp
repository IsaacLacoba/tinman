// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "car.h"

Car::Car(std::string nick) {
  nick_ = nick;
  accelerating_ = false;
  controller_ = std::make_shared<CarController>();
  lap_ = 0;
  current_segment_ = Section{};

  action_hooks[Tinman::Action::AccelerateBegin] =
            std::bind(&Car::accelerate, this);
  action_hooks[Tinman::Action::AccelerateEnd] =
            std::bind(&Car::stop_accelerating, this);
  action_hooks[Tinman::Action::BrakeBegin] =
            std::bind(&Car::brake, this);
  action_hooks[Tinman::Action::BrakeEnd] =
            std::bind(&Car::stop_braking, this);
  action_hooks[Tinman::Action::TurnRight] =
            std::bind(&Car::turn_right, this);
  action_hooks[Tinman::Action::TurnLeft] =
            std::bind(&Car::turn_left, this);
  action_hooks[Tinman::Action::TurnEnd] =
            std::bind(&Car::stop_turning, this);
  action_hooks[Tinman::Action::UseNitro] =
    std::bind(&Car::use_nitro, this);

  current_segment_ = Section{};
  sections_covered_ = 0;
  colliding_ = stuck_ = can_collide_ = false;
  colliding_time_ = invert_direction_ = not_moving_ = 0.f;
  std::cout << "[Car] constructor" << std::endl;
}

Car::~Car() {
  for (int j=0;j<collision_shapes_.size();j++)
    {
      btCollisionShape* shape = collision_shapes_[j];
      delete shape;
    }
}

void
Car::exec(Tinman::Action action) {
  // std::cout << "[car] exec: " << action << std::endl;
  action_hooks[action]();
}

void
Car::realize(Scene::shared scene, std::string name, std::string material,
             Ogre::Vector3 position, int scale) {
  init_graphic_bodies(scene, name, material, position, scale);
  wheels_nodes_[0]->translate(1, -0.2, 1);
  wheels_nodes_[1]->translate(-1, -0.2, 1);
  wheels_nodes_[2]->translate(1, -0.2, -1);
  wheels_nodes_[3]->translate(-1, -0.2, -1);
}

void
Car::realize(Physics::shared physics, Sound::shared sound, std::string name,
             btVector3 position, Ice::Byte id) {
  physics_ = physics;
  sound_ = sound;
  id_ = id;

  init_physic_bodies(physics, position);
  init_raycast_car(physics);

  add_wheels();
  configure_wheels();
  controller_->realize( wheels_nodes_);
}

void
Car::realize(Physics::shared physics, Scene::shared scene,
             Sound::shared sound,  std::string name, btVector3 position,
             std::string material, int checkpoints, Ice::Byte id) {
  int index = 0;
  checkpoints_ = std::vector<std::pair<int, bool>>(checkpoints, {index++, false});

  init_graphic_bodies(scene, name, material);
  realize(physics, sound, name, position, id);
}

void
Car::init_graphic_bodies(Scene::shared scene, std::string name, std::string material,
                         Ogre::Vector3 position, int scale) {
  chassis_node_ = scene->get_child_node("", name);
  chassis_node_->setPosition(position);

  scene->get_particle(chassis_node_, "car_smoke" + id_,  "Bomb/Smoke");
  chassis_entity_ = scene->create_entity(name, "car.mesh", true);
  if(material != "")
    chassis_entity_->setMaterialName(material);
  scene->attach(chassis_node_,chassis_entity_);

  for (int i = 0; i < 4; ++i) {
    std::stringstream wheel_name;
    wheel_name << name << "wheel" << i;
    add_graphic_wheel(scene, name, wheel_name.str());
  }
  chassis_node_->setScale(scale, scale, scale);
}

void
Car::init_physic_bodies(Physics::shared physics, btVector3 position) {
  btTransform tr;
  tr.setIdentity();

  btVector3 origin = btVector3(0, 1, 0);
  compound_ = physics->
    create_compound_shape(origin, physics->create_shape(controller_->car_dimensions_));
  collision_shapes_.push_back(compound_);

  initial_position_ = position;
  chassis_body_ =  physics->
    create_rigid_body(btTransform( initial_rotation_,position),
                      chassis_node_, compound_, controller_->mass_);
  chassis_body_->setDamping(0.2,0.2);
  chassis_body_->setActivationState(DISABLE_DEACTIVATION);
}

void
Car::init_raycast_car(Physics::shared physics) {
  vehicle_raycaster_ = new btDefaultVehicleRaycaster(physics->dynamics_world_);
  vehicle_ = new btRaycastVehicle(tuning_ , chassis_body_, vehicle_raycaster_);

  physics->dynamics_world_->addVehicle(vehicle_);
}

void
Car::add_nick_billboard(Scene::shared scene) {
  // std::stringstream nick_node;
  // nick_node << uuid_ << "node";
  // nickname_display_ = new ObjectTextDisplay(chassis_entity_, scene->camera_);
  // nickname_display_->enable(true);
  // nickname_display_->setText(uuid_);
}

void
Car::add_graphic_wheel(Scene::shared scene, std::string parent, std::string name, int scale) {
  wheels_nodes_.push_back(scene->get_child_node(parent, name));

  std::string mesh = "wheel.mesh";
  wheels_entities_.push_back(scene->create_entity(name, mesh, true));

  scene->attach(wheels_nodes_.back(), wheels_entities_.back());
  for(auto wheel: wheels_nodes_)
    wheel->setScale(scale, scale, scale);
}

void
Car::add_physic_wheel(bool is_front, btVector3 connection_point, int wheel_index) {
  vehicle_->addWheel(connection_point, controller_->wheel_direction_cs0_,
                     controller_->wheel_axle_cs_, controller_->suspension_rest_length_,
                     controller_->wheel_radius_,
                     tuning_ , is_front);
  wheels_nodes_[wheel_index]->translate(connection_point.getX() ,
                                        -0.3,
                                        connection_point.getZ());
}

void
Car::add_wheels() {
  const btScalar lateral_correction = (0.3 * controller_->wheel_width_);
  btScalar left_wheel = controller_->car_dimensions_.getX() - lateral_correction;
  btScalar right_wheel = -controller_->car_dimensions_.getX() + lateral_correction;
  btScalar front_wheel = controller_->car_dimensions_.getZ() - controller_->wheel_radius_;
  btScalar rear_wheel = -controller_->car_dimensions_.getZ() + controller_->wheel_radius_;

  add_physic_wheel(true, btVector3(left_wheel,   controller_->connection_height_,
                                   front_wheel),0);
  add_physic_wheel(true, btVector3(right_wheel,  controller_->connection_height_,
                                   front_wheel), 1);
  add_physic_wheel(false, btVector3(right_wheel, controller_->connection_height_,
                                    rear_wheel), 2);
  add_physic_wheel(false, btVector3(left_wheel,  controller_->connection_height_,
                                    rear_wheel), 3);
}

void
Car::configure_wheels(){
  for (int i=0; i < vehicle_->getNumWheels();i++) {
    btWheelInfo& wheel = vehicle_->getWheelInfo(i);
    wheel.m_suspensionStiffness = controller_->suspension_stiffness_;
    wheel.m_wheelsDampingRelaxation = controller_->suspension_damping_;
    wheel.m_wheelsDampingCompression = controller_->suspension_compression_;
    wheel.m_frictionSlip = controller_->wheel_friction_;
    wheel.m_rollInfluence = controller_->roll_influence_;
  }
}

void
Car::accelerate() {
  // std::cout << "[car] " << __func__ <<std::endl;
  accelerating_ = true;
  controller_->accelerate();
}

void
Car::stop_accelerating() {
  accelerating_ = false;
  controller_->stop_accelerating();
}

void
Car::brake() {
  accelerating_ = false;
  controller_->brake();
}

void
Car::stop_braking() {
  accelerating_ = false;
  controller_->stop_braking();
}

void
Car::turn(Direction direction) {
  controller_->turn(direction);
}

void
Car::turn_right() {
  controller_->turn(Direction::right);
}

void
Car::turn_left() {
  controller_->turn(Direction::left);
}

void
Car::stop_turning() {
  controller_->stop_turning();
}

void
Car::control_speed() {
  controller_->control_speed();
}

void
Car::update(float delta) {
  update_lap();
  for (int wheel = 0; wheel < 4; ++wheel)
    vehicle_->applyEngineForce(controller_->f_engine_, wheel);

  for (int wheel = 0; wheel < 2; ++wheel)
    vehicle_->setSteeringValue(controller_->steering_, wheel);

  // FIXME: refactor!!! smelly code
  invert_direction_ += delta;

  not_moving_ =  not_moving()? not_moving_ + delta: 0.f;
  stuck_ = not_moving_ > not_moving_delay_;
  std::cout << "accelerating: " << accelerating_
            << "not moving: " << not_moving_
            << " delay: " << not_moving_delay_ << std::endl;

  if(accelerating_) {
    colliding_time_ = colliding_ ? colliding_time_ + delta: 0.f;
    can_collide_ = colliding_time_ > colliding_delay_? true: false;
    // std::cout << "colliding time: " << colliding_time_
    // 	      << " can collide: " << can_collide_
    // 	      << " colliding_delay: " << colliding_delay_ << std::endl;
  }

  controller_->update(delta);
}

void
Car::update_lap() {
  if(!checkpoints_[current_segment_.id].second) {
    checkpoints_[current_segment_.id].second = true;
    sections_covered_++;
  }

  if(is_on_meta() && has_done_a_lap()) {
    std::cout << "lap" << std::endl;
    add_lap();
  }

  race_position_ = current_segment_.id + (lap_ + 1) * checkpoints_.size() * 2;

}

void
Car::synchronize(Tinman::CarInfo last_update) {
  sync_buffer_.push(last_update);

  last_position_ = btVector3(sync_buffer_.top().position.x,
                             sync_buffer_.top().position.y,
                             sync_buffer_.top().position.z);

  last_orientation_ = btQuaternion(sync_buffer_.top().orientation.x,
                                   sync_buffer_.top().orientation.y,
                                   sync_buffer_.top().orientation.z,
                                   sync_buffer_.top().orientation.w);

  last_velocity_ = btVector3(sync_buffer_.top().velocity.x,
                             sync_buffer_.top().velocity.y,
                             sync_buffer_.top().velocity.z);
  apply_update(true);
}

void
Car::apply_update(bool delete_update) {
  physics_->set_transform(chassis_body_, btTransform(last_orientation_, last_position_));
  chassis_body_->setLinearVelocity( last_velocity_);

  if(delete_update)
    sync_buffer_.pop();
}

bool
equals(float sut, float target, float delta)  {
  return (sut == target) ||
    ((sut <= target + delta) &&
    (sut >= target - delta));
}

bool
equals(btVector3 sut, btVector3 target, float delta) {
  return equals(sut.getX(), target.getX(),  delta) &&
    equals(sut.getZ(), target.getZ(), delta);
}

btVector3
Car::get_position() {
  return chassis_body_->getCenterOfMassPosition();
}

btVector3
Car::get_velocity() {
  return chassis_body_->getLinearVelocity();
}

btQuaternion
Car::get_orientation() {
  return chassis_body_->getOrientation();
}

int
Car::get_nitro(){
  return controller_->nitro_;
}

int
Car::get_lap(){
  return lap_;
}

void
Car::use_nitro() {
  controller_->use_nitro();
}

void
Car::add_lap() {
  for(auto& checkpoint: checkpoints_) {
    checkpoint.second = false;
  }
  sections_covered_ = 0;
  ++lap_;
}

void
Car::reset(int checkpoints, btVector3 position) {
  int id = 0;
  checkpoints_ = std::vector<std::pair<int, bool>>(checkpoints, {id++, false});
  set_position(position);

  lap_ = 0;
  sections_covered_ = 0;
  chassis_body_->setLinearVelocity(btVector3(0.f, 0.f, 0.f));
  controller_->f_engine_ = 0.f;
  controller_->f_braking_  = 0.f;

  while(!sync_buffer_.empty()) {
    sync_buffer_.pop();
  }
}

void
Car::reset_position() {
  physics_->set_transform(chassis_body_,
                          btTransform(initial_rotation_, initial_position_));
}

void
Car::set_race_segment_info(Section segment) {
  current_segment_ = segment;
}

btVector3
Car::get_direction(int factor) {
  btTransform chassis_transform = vehicle_->getChassisWorldTransform();
  btScalar chassis_matrix[16];
  chassis_transform.getOpenGLMatrix(chassis_matrix);

  return btVector3(chassis_matrix[8], chassis_matrix[9], chassis_matrix[10])
    * factor + get_position();
}

bool
Car::is_on_meta() {
  return current_segment_.type == Section::Type::Meta &&
    equals(get_position().z(), current_segment_.position.z(), 8) &&
    equals(get_position().x(), current_segment_.position.x(), 2);
}

bool
Car::has_done_a_lap() {
  return sections_covered_ == checkpoints_.size();
}

btScalar
Car::get_speed() {
  return vehicle_->getCurrentSpeedKmHour();
}

Section
Car::get_current_section() {
  return current_segment_;
}

void
Car::animation(float delta) {
  chassis_node_->yaw(Ogre::Angle(delta*15));
  for(auto& wheel: wheels_nodes_)
    wheel->pitch(Ogre::Angle(delta*100));
}

bool
Car::is_colliding() {
  return is_stuck();
    // can_collide_? colliding_: false;
}

bool
Car::not_moving() {
  return (accelerating_ && (abs(get_velocity().x()) <= 0.1) && (abs(get_velocity().z()) <= 0.1));
}

bool
Car::is_stuck() {
  return stuck_;
}

void
Car::collide_with_car() {
  // std::cout << "[Car] " << __func__ << std::endl;
  colliding_ = true;
}

void
Car::set_position(btVector3 position) {
  physics_->set_transform(chassis_body_,
                          btTransform(initial_rotation_, position));
}

void
Car::invert_direction() {
  // if(invert_direction_ < invert_direction_delay_)
  //   return;
  // invert_direction_ = 0.f;
  std::cout << "[Car] " << __func__ << std::endl;
  controller_->inverted_ = !controller_->inverted_;
}

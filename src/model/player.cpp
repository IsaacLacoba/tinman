// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "player.h"
#include <Ice/Ice.h>


Player::Player(std::string nick, Controller::shared controller, Sound::shared sound,
	       Animation::shared animation) {
  std::cout << "[Player] constructor:" << nick <<std::endl;
  nick_ = nick;
  sound_ = sound;
  controller_ = controller;
  money_ = 20000;
}

Player::~Player() {
}

Ice::AsyncResultPtr
Player::login(std::shared_ptr<Session> session) {
  return controller_->observer_?
    controller_->observer_->login(session): nullptr;

}

Tinman::LoginInfo
Player::complete_login(Ice::AsyncResultPtr async_result) {
  return controller_->observer_?
    controller_->observer_->complete_login(async_result) : Tinman::LoginInfo{};
}

Ice::AsyncResultPtr
Player::next_race() {
  return controller_->observer_?
    controller_->observer_->next_race(get_nick()): nullptr;
}
void
Player::realize(Physics::shared physics, Scene::shared scene,
                std::string  name, btVector3 position,
                std::string material, int checkpoints, Ice::Byte id) {
  std::cout << "[Player] realize" << std::endl;
  scene_ = scene;
  get_car()->realize(physics, scene, sound_, name, position,  material, checkpoints, id);
}

void
Player::update(float delta) {
  controller_->update(delta);
}

void
Player::disconnect( ) {
  std::cout << "[Player] " << __func__ << std::endl;
    if(controller_->observer_)
      controller_->observer_->disconnect();
}

Car::shared
Player::get_car(){
  return controller_->car_;
}

std::string
Player::get_nick() {
  return nick_;
}

void
Player::set_nick(std::string nick) {
  nick_ = nick;
}

Ice::Byte
Player::get_id() {
  return controller_->car_->id_;
}

void
Player::store(Tinman::CarInfo car_info) {
  controller_->store(std::move(car_info));
}

void
Player::purchase() {
  get_car()->controller_->nitro_ += 10;
  money_ -= 10000;
}

void
Player::pick_money(int amount, PowerUp::shared money) {
  // add_animation("$.mesh", "scaling");
  sound_->play("media/sound/key_pickup.wav");
  money_ += amount;
  money->pick_up();
}

void
Player::pick_nitro(PowerUp::shared nitro) {
  // add_animation("+1.mesh", "scaling");
  sound_->play("media/sound/key_pickup.wav");
  get_car()->controller_->nitro_ += 10;
  nitro->pick_up();
}

void
Player::reset(int checkpoints, btVector3 position) {
  get_car()->reset(checkpoints, position);
  while(!controller_->updates_.empty())
    controller_->updates_.pop();
}

std::pair<std::string, int>
Player::get_socket() {
  return controller_->observer_->socket_;
}

void
Player::set_socket(std::pair<std::string, int> socket) {
  controller_->observer_->socket_ = socket;
}

void
Player::add_animation(std::string mesh, std::string animation) {
  auto nitro_anim = scene_->create_graphic_element("player_anim", mesh,
				   get_car()->chassis_node_->getName(), mesh);
  auto animation_entity_ = static_cast<Ogre::Entity*>(nitro_anim->getAttachedObject(0));

  animation_->activate(animation_entity_, animation,
		       std::bind(&Player::clean_animation, this));
}

void
Player::clean_animation() {
  scene_->destroy_node("player_anim");
}

// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef TIMER_H
#define TIMER_H
#include <chrono>
#include <cmath>

class Timer {
 public:
  typedef std::chrono::steady_clock::duration DeltaTime;
  typedef std::chrono::steady_clock::time_point Time;

  Timer();
  virtual ~Timer();
  float get_delta_time();
  void start();
  void restart();
  void stop();

  float get_time_since_start();

 private:
  Time last_time_, stop_;
  DeltaTime  delta_, freeze_time_;
  float start_;

  float time_to_float(DeltaTime time);
  float truncate(float number);
  Time now();
};

#endif

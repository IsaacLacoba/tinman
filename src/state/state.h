// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STATE_HPP
#define STATE_HPP
#include <memory>

#include "scene.h"
#include "input.h"
#include "gui.h"

class Game;

class State {
 protected:
  std::shared_ptr<Game> game_;

public:
  typedef std::shared_ptr<State> shared;

  State(std::shared_ptr<Game> game);
  virtual ~State();

  virtual void enter() = 0;
  virtual void update(float delta) = 0;
};

#endif

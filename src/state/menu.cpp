// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"
#include "car.h"

Menu::Menu(std::shared_ptr<Game> game): State(game) {
  autologin_ = false;
}

Menu::~Menu() {
  delete menu_window_;
}

void
Menu::enter() {
  std::cout << "[Menu] enter" << std::endl;
  init_gui();
  add_hooks();
  add_gui_hooks();
  if(autologin_)
    autologin();
}

void
Menu::init_gui() {
  load_window();
  create_scene();
  // background_->show();
}

void
Menu::autologin() {
  menu_window_->hide();
  menu_window_->deactivate();
  mode_window_->hide();
  mode_window_->deactivate();
  game_->get_human_player()->nick_ = autologin_ ? IceUtil::generateUUID() :
    config_window_->getChild("Nickname")->getText().c_str();

  game_->ip_ = config_window_->getChild("IpServer")->getText().c_str();
  game_->port_ = std::stoi(config_window_->getChild("PortServer")->getText().c_str());

  login_ = game_->login();
  if(!login_) {
    config_window_->getChild("Error")->setText("Error: no server found");
    return;
  }

  config_window_->hide();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(waiting_window_);
  waiting_window_->activate();
  waiting_window_->setVisible(true);
}

void
Menu::load_window() {
  config_window_ = game_->gui_->load_layout("NetConfig.layout");
  config_window_->deactivate();

  waiting_window_ = game_->gui_->load_layout("Waiting.layout");
  waiting_window_->deactivate();

  credits_window_ = game_->gui_->load_layout("Credits.layout");
  credits_window_->deactivate();

  mode_window_ = game_->gui_->load_layout("PlayMode.layout");
  mode_window_->deactivate();

  menu_window_ = game_->gui_->load_layout("Menu.layout");
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);
  menu_window_->activate();
}

bool
Menu::show_login(const CEGUI::EventArgs &event) {
  menu_window_->hide();
  menu_window_->deactivate();

  config_window_->show();
  config_window_->activate();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(config_window_);

  return true;
}

bool
Menu::show_menu(const CEGUI::EventArgs &event) {
  mode_window_->hide();
  mode_window_->deactivate();

  config_window_->hide();
  config_window_->deactivate();

  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(menu_window_);
  menu_window_->show();
  menu_window_->activate();
  return true;
}

void
Menu::hide_waiting_screen() {
  waiting_window_->hide();
  waiting_window_->deactivate();

}

bool
Menu::show_credits(const CEGUI::EventArgs &event) {
  menu_window_->hide();
  menu_window_->deactivate();

  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(credits_window_);
  credits_window_->show();
  credits_window_->activate();

  return true;
}

bool
Menu::show_mode_window(const CEGUI::EventArgs &event) {
  config_window_->getChild("Error")->setText("");
  config_window_->hide();
  config_window_->deactivate();

  menu_window_->deactivate();

  mode_window_->show();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(mode_window_);
  mode_window_->activate();

  return true;
}

bool
Menu::login(const CEGUI::EventArgs &event) {
  autologin();
  return true;
}

void
Menu::update(float delta) {
  game_->gui_->inject_delta(delta);
  if(expose_car_)
    expose_car_->animation(delta);
  if(login_ && login_->isCompleted()) {
    complete_login();
  }
}

void
Menu::add_hooks() {
  game_->input_->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                          std::bind(&Game::shutdown, game_.get()));
}

void
Menu::add_gui_hooks() {
  add_hooks(menu_window_, "MenuPlay",
	    CEGUI::Event::Subscriber(&Menu::show_mode_window, this));
  add_hooks(menu_window_, "MenuCredits",
	    CEGUI::Event::Subscriber(&Menu::show_credits, this));
  add_hooks(menu_window_, "MenuExit",
	    CEGUI::Event::Subscriber(&Game::gui_shutdown, game_.get()));
  add_hooks(config_window_, "Login",
	    CEGUI::Event::Subscriber(&Menu::login, this));
  add_hooks(config_window_, "NetConfigBack",
	    CEGUI::Event::Subscriber(&Menu::show_mode_window, this));
  add_hooks(waiting_window_, "WaitingBack",
	    CEGUI::Event::Subscriber(&Menu::disconnect, this));
  add_hooks(credits_window_, "CreditsBack",
	    CEGUI::Event::Subscriber(&Menu::show_menu, this));
  add_hooks(mode_window_, "Bots", CEGUI::Event::Subscriber(&Menu::change_bots_mode,
							   this));
  add_hooks(mode_window_, "Multiplayer", CEGUI::Event::Subscriber(&Menu::show_login,
								  this));
  add_hooks(mode_window_, "Back", CEGUI::Event::Subscriber(&Menu::show_menu, this));
}

void
Menu::add_hooks(CEGUI::Window* window, const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  window->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

bool
Menu::change_bots_mode(const CEGUI::EventArgs &event) {
  game_->mode_ = Game::Mode::Bots;
  game_->scene_->remove_child("", "expose_car");
  game_->change_state();
  // background_->hide();
  mode_window_->hide();
  return true;
}

void
Menu::complete_login() {
  std::cout << "se ha recibido el login en Clase Menu" << std::endl;
  game_->scene_->remove_child("", "expose_car");
  game_->complete_login(login_);
  game_->change_state();
  // background_->hide();
}


void
Menu::create_scene() {
  // expose_car_ = std::make_shared<Car>("expose_car");
  // expose_car_->realize(game_->scene_, "expose_car", "", Ogre::Vector3(40, 37, 0), 4);
  // expose_car_->chassis_node_->pitch(Ogre::Degree(-30));

  expose_track_ = std::make_shared<Track>("config/circuit1.data");
  expose_track_->next(game_->scene_, game_->physics_);
}

bool
Menu::disconnect(const CEGUI::EventArgs &event) {
  std::cout << "[Menu] " << __func__ << std::endl;
  game_->disconnect();
  show_mode_window(CEGUI::EventArgs());
  return true;
}

// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MENU_H
#define MENU_H
#include "state.h"
#include <Ice/Ice.h>

class Car;

class Menu: public State {
  CEGUI::Window* menu_window_, *credits_window_,
    *mode_window_, *config_window_,  *waiting_window_;
  Ice::AsyncResultPtr login_;
  bool autologin_;
  Ogre::OverlayElement *background_;
  std::shared_ptr<Car> expose_car_;

 public:
  typedef std::shared_ptr<Menu> shared;

  std::shared_ptr<Track> expose_track_;

  Menu(std::shared_ptr<Game> game);
  virtual ~Menu();

  void enter();
  void update(float delta);

  void hide_waiting_screen();

 private:
  bool show_login(const CEGUI::EventArgs &event);
  bool show_menu(const CEGUI::EventArgs &event);
  bool show_credits(const CEGUI::EventArgs &event);
  bool show_mode_window(const CEGUI::EventArgs &event);
  bool change_bots_mode(const CEGUI::EventArgs &event);

  void add_hooks();
  void add_hooks(CEGUI::Window* window, const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
  void add_gui_hooks();
  void init_gui();
  void load_window();

  void autologin();
  bool login(const CEGUI::EventArgs &event);
  bool disconnect(const CEGUI::EventArgs &event);

  void create_scene();
  void complete_login();
};
#endif

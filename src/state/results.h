#ifndef RESULTS_H
#define RESULTS_H
#include "state.h"

class Results: public State {
  CEGUI::Window* menu_window_, *shop_window_, *waiting_window_;
  bool is_init_;
  Ice::AsyncResultPtr next_race_;
  float last_nitro_;
public:
  typedef std::shared_ptr<Results> shared;
  Results(std::shared_ptr<Game> game);
  virtual ~Results();

  void enter();
  void update(float delta);

 private:
  void switch_menu();
  void add_hooks();
  void add_gui_hooks();
  void add_hooks(CEGUI::Window* window, const std::string& button,
                 const CEGUI::Event::Subscriber& callback);
  bool change_state(const CEGUI::EventArgs &event);
  bool show_shop(const CEGUI::EventArgs &event);
  void show_results();
  void config_progress_bar();

  bool sell_nitro(const CEGUI::EventArgs &event);
};
#endif

// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
//   Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "game.h"

Play::Play(std::shared_ptr<Game> game): State(game) {
}

Play::~Play() {
}

void
Play::enter() {
  std::cout << "[Play] enter" << std::endl;
  game_->session_->race_->config_race();

  if(!game_->race_started_ && !game_->debug_) {
    animation_node_ = game_->scene_->create_graphic_element(
                       Ogre::Vector3(40, 10, -10), "go_", "go.mesh");
    animation_entity_ = static_cast<Ogre::Entity*>
    (game_->scene_->get_node("go_")->getAttachedObject(0));

    game_->animation_->activate(animation_entity_, "scaling",
                              std::bind(&Play::config_race, this));
    game_->session_->race_->start();
    game_->race_started_ = true;
  }

  if(!game_->get_human_player())
    return;
  game_->get_human_player()->controller_->configure();
  add_hooks();
}

void
Play::config_race() {
  std::cout << "[Play] configurando el circuito" << std::endl;
  game_->scene_->destroy_node(animation_node_);
  game_->session_->race_->start();
  game_->session_->human_player_->controller_->configure();
  add_hooks();
}

void
Play::add_hooks() {
  game_->input_->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed}, EventType::doItOnce,
                          std::bind(&Game::change_state, game_));
}

void
Play::update(float delta) {
  game_->session_->update(delta);
}

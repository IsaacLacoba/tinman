// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "statemachine.h"

Game::Game() {
  debug_ = false;
  init();
  std::pair<std::string, int> socket = {ip_, port_};
  session_ = std::make_shared<Session>(std::make_shared<Race>("config/circuit1.data"),
	                     HumanFactory::create("Tinman", input_, sound_, animation_,
                                            car_factory_->create("Tinman")));
}

Game::Game(Session::shared session) {
  debug_ = true;
  add_debug_adapter();
  session_ = session;
  init();
}

void
Game::init() {
  scene_ = std::make_shared<Scene>();
  physics_ = std::make_shared<Physics>();
  input_ = std::make_shared<EventListener>(scene_->window_);
  gui_ = std::make_shared<GUI>();
  sound_ = std::make_shared<Sound>();
  animation_ = std::make_shared<Animation>();
  car_factory_ = debug_adapter_ ? std::make_shared<CarFactory>(debug_adapter_):
                                  std::make_shared<CarFactory>();

  mode_ = Game::Mode::Network;
  ip_ = "1";
  port_ = 1;
  nick_ = "Tinman";
  race_started_ = false;
}

Game::~Game() {
}

void
Game::change_state() {
  state_machine_->change_state();

}

void
Game::set_current_state(StateName state) {
  state_machine_->set_current_state(state);
}


void
Game::realize() {
  session_->game_ = shared_from_this();
  state_machine_ = std::make_shared<StateMachine>(shared_from_this());
}

void
Game::start(int seconds) {
  realize();
  session_->realize();
  state_machine_->set_current_state(StateName::Play);
  debug_adapter_->activate();
  game_loop(seconds);
}

void
Game::start() {
  realize();
  state_machine_->set_current_state(StateName::Menu);

  game_loop();
}

Ice::AsyncResultPtr
Game::login() {
  return session_->login();
}

void
Game::complete_login(Ice::AsyncResultPtr async_result) {
  session_->complete_login(async_result);
}

void
Game::disconnect() {
  session_->disconnect();
}

void
Game::game_loop() {
  timer_.start();
  while(!input_->exit_) {
    step();
  }
}

void
Game::game_loop(int frames) {
  timer_.start();
  frame_counter_ = 0;
  while(frame_counter_ < frames) {
    step();
  }
}

void
Game::step() {
  delta_ += timer_.get_delta_time();
  input_->capture();
  if(delta_ >= (1/FPS)) {
    input_->check_events();
    state_machine_->get_current_state()->update(delta_);
    scene_->render_one_frame();
    if(physics_drawer_)
      physics_drawer_->step();

    delta_ = 0.f;
    frame_counter_++;
  }
}

void
Game::shutdown() {
  input_->shutdown();
}

bool
Game::gui_shutdown(const CEGUI::EventArgs &event) {
  input_->shutdown();
  return true;
}

Player::shared
Game::get_human_player() {
  return session_->human_player_;
}

void
Game::add_debug_adapter() {
  Ice::StringSeq argv{"Instrumentated-Tinman"};
  communicator_ = Ice::initialize(argv);
  debug_adapter_ = communicator_->createObjectAdapterWithEndpoints("DebugAdapter", "tcp -h 127.0.0.1 -p 10000");
}

void
Game::add_debug_drawer() {
  physics_drawer_ = std::make_shared<PhysicsDebugDrawer>(scene_->get_node(""),
						  physics_->dynamics_world_);

  physics_drawer_->setDebugMode(btIDebugDraw::DBG_DrawWireframe |
				btIDebugDraw::DBG_DrawAabb |
			      btIDebugDraw::DBG_DrawContactPoints);

  physics_->dynamics_world_->setDebugDrawer(physics_drawer_.get());
}

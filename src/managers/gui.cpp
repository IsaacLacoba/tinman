// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "gui.h"

GUI::GUI(){
  CEGUI::OgreRenderer::bootstrapSystem();
  overlay_manager_ = Ogre::OverlayManager::getSingletonPtr();
  load_resources();
  init();
}

GUI::~GUI(){
  delete overlay_manager_;
}


void
GUI::load_resources() {
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
}

void
GUI::init() {
  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setDefaultFont(default_font_);
  context_.getMouseCursor().setPosition(CEGUI::Vector2f(0, 0));
}

CEGUI::Window*
GUI::load_layout(std::string file) {
  return CEGUI::WindowManager::getSingleton().loadLayoutFromFile(file);
}

CEGUI::AnimationInstance*
GUI::load_animation(std::string name, CEGUI::Window* window) {
  CEGUI::AnimationManager& animation_manager_ = CEGUI::AnimationManager::getSingleton();
  CEGUI::Animation* animation = animation_manager_.getAnimation(name);
  CEGUI::AnimationInstance* instance =
    CEGUI::AnimationManager::getSingleton().instantiateAnimation(animation);
  instance->setTargetWindow(window);

  return instance;
}

Ogre::OverlayElement*
GUI::create_overlay(std::string name, std::string element){
  Ogre::Overlay *overlay = overlay_manager_->getByName(name);
  overlay->show();
  return overlay_manager_->getOverlayElement(element);
}

Ogre::OverlayElement*
GUI::create_overlay(std::string name, std::string element,
                    std::pair<float, float> position,
                    std::pair<float, float> dimension) {
  Ogre::Overlay* overlay = overlay_manager_->create( name );

  Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>( overlay_manager_->createOverlayElement( "Panel", element) );
  panel->setPosition( position.first, position.second );
  panel->setDimensions( dimension.first, dimension.second);

  overlay->add2D( panel );

  std::stringstream text_overlay;
  text_overlay << name << "Text";
  Ogre::OverlayElement* text_box_ =
    overlay_manager_->createOverlayElement("TextArea", text_overlay.str());

  text_box_->setDimensions(dimension.first, dimension.second);
  text_box_->setMetricsMode(Ogre::GMM_PIXELS);
  text_box_->setPosition(position.first, position.second);
  text_box_->setWidth(dimension.first);
  text_box_->setHeight(dimension.second);
  text_box_->setParameter("font_name", "Manila");
  text_box_->setParameter("char_height", "50");
  text_box_->setColour(Ogre::ColourValue::White);

  panel->addChild(text_box_);

  overlay->show();

  return text_box_;
}

CEGUI::GUIContext&
GUI::get_context() {
    return CEGUI::System::getSingleton().getDefaultGUIContext();
}

void
GUI::inject_delta(float delta) {
  CEGUI::GUIContext& defaultGUIContext(get_context());
  defaultGUIContext.injectTimePulse(delta);
}

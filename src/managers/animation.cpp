// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include "animation.h"
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

Animation::Animation() {

}

Animation::~Animation() {

}

void
Animation::activate(Ogre::Entity* entity, std::string animation_name) {
  Ogre::AnimationState* animation = entity->getAnimationState(animation_name);

  if(active_animations_[animation])
    return;

  animation->setEnabled(true);
  animation->setTimePosition(0.0);
}

void
Animation::activate(Ogre::Entity* entity, std::string animation_name,
      Animation::Callback callback) {

   Ogre::AnimationState* animation = entity->getAnimationState(animation_name);

  if(active_animations_[animation])
    return;

  animation->setEnabled(true);
  animation->setLoop(false);
  animation->setTimePosition(0.0);

  active_animations_[animation] = callback;
}

void
Animation::update(float deltaT) {
  std::vector<Ogre::AnimationState*> for_delete;

  for (auto& animation: active_animations_) {
    animation.first->addTime(deltaT);

    if (animation.first->hasEnded())
      for_delete.push_back(animation.first);
  }

  for(auto& animation: for_delete) {
    active_animations_[animation]();
    active_animations_.erase(animation);
  }
}

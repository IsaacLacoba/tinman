// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Tinman author: Isaac Lacoba Molina
// Copyright (C) 2014  ISAAC LACOBA MOLINA
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "scene.h"

Scene::Scene() {
  Ogre::LogManager* logMgr = OGRE_NEW Ogre::LogManager;
  logMgr->createLog("config/ogre.log", true, false, false);
  root_ = new Ogre::Root("config/plugins.cfg", "config/ogre.cfg", "");

  if (not root_->restoreConfig() )
    root_->showConfigDialog();

  window_ = root_->initialise(true, window_title);

  scene_manager_ = root_->createSceneManager(Ogre::ST_GENERIC);

  create_camera(window_);
  create_light();

  load_resources();
}

void
Scene::load_resources() {
  Ogre::ConfigFile cf;
  cf.load("config/resources.cfg");

  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

  Ogre::String secName, typeName, archName;
  while (seci.hasMoreElements()) {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typeName = i->first;
      archName = i->second;
      Ogre::ResourceGroupManager::getSingleton()
        .addResourceLocation(archName, typeName, secName);
    }
  }

  Ogre::ResourceGroupManager::getSingleton()
    .initialiseAllResourceGroups();
}

void
Scene::render_one_frame(void) {
  Ogre::WindowEventUtilities::messagePump();
  root_->renderOneFrame();
}

Ogre::Ray
Scene::set_ray_query(float x, float y) {
  Ogre::Ray ray = camera_->
    getCameraToViewportRay(x/float(window_->getWidth()), y/float(window_->getHeight()));

  ray_query_->setRay(ray);

  return ray;
}

void
Scene::create_light() {
  scene_manager_->setAmbientLight(Ogre::ColourValue(0.4, 0.4, 0.4));
  scene_manager_->setShadowTextureCount(2);
  scene_manager_->setShadowTextureSize(512);
  scene_manager_->setShadowColour(Ogre::ColourValue(0.2, 0.2, 0.2));
  scene_manager_->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

  Ogre::Light* light2 = scene_manager_->createLight("2Light");
  light2->setCastShadows(true);
  light2->setPosition(40, 30, 10);
  light2->setType(Ogre::Light::LT_POINT);
  light2->setDiffuseColour(15, 15 , 15);
  light2->setSpecularColour(15, 15 , 15);
}

void
Scene::create_camera(Ogre::RenderWindow* window) {
  Ogre::SceneNode* camera_node = create_node("camera_node");
  camera_ = scene_manager_->createCamera("PlayerCamera");
  camera_->setPosition(Ogre::Vector3(40 , 70, 35));
  camera_->lookAt(Ogre::Vector3(40, 0, -30));
  camera_->setNearClipDistance(20);
  camera_->setFarClipDistance(400);

  Ogre::Viewport* viewport = window->addViewport(camera_);
  viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  camera_->setAspectRatio(Ogre::Real(viewport->getActualWidth()) /
                          Ogre::Real(viewport->getActualHeight()));
  camera_node->attachObject(camera_);
}

Ogre::SceneNode*
Scene::create_node(std::string name) {
  return scene_manager_->createSceneNode(name);
}

Ogre::SceneNode*
Scene::get_node(std::string node) {
  if(node == "")
    return scene_manager_->getRootSceneNode();

  if(scene_manager_->hasSceneNode(node))
    return scene_manager_->getSceneNode(node);

  return nullptr;
}

void
Scene::attach(Ogre::SceneNode* node, Ogre::Entity* entity) {
  node->attachObject(entity);
}

Ogre::SceneNode*
Scene::create_graphic_element(Ogre::Vector3 position, std::string name, std::string mesh) {
  Ogre::SceneNode* ground_node = create_graphic_element(name, mesh, "", name);
  ground_node->setPosition(position);
  return ground_node;
}

Ogre::SceneNode*
Scene::create_graphic_element(std::string entity, std::string mesh,
                                 std::string parent, std::string name) {
  return create_graphic_element(create_entity(entity, mesh, true), parent, name);
}

Ogre::SceneNode*
Scene::create_graphic_element(Ogre::Entity* entity, std::string parent, std::string name) {
  Ogre::SceneNode* node = get_child_node(parent, name);
  attach(node, entity);

  return node;
}

Ogre::SceneNode*
Scene::create_plane(std::string axis, std::string name, std::string mesh,
        std::string parent, std::string material) {
  Ogre::Plane planeGround(get_axis(axis), -1);
  Ogre::MeshManager::getSingleton().createPlane(mesh,
  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, planeGround,
  800,800, 1, 1, true, 1, 8, 8,get_normal(axis));

  Ogre::SceneNode* ground_node = get_child_node(parent, name);
  Ogre::Entity* ground_entity = create_entity(name, mesh, false);

  ground_entity->setMaterialName(material);
  ground_node->attachObject(ground_entity);

  return ground_node;
}

Ogre::SceneNode*
Scene::get_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = get_node(name);
  if(node == nullptr)
      node = create_child_node(parent, name);

  return node;
}

Ogre::SceneNode*
Scene::get_child_node(std::string parent, std::string name) {
  return get_child_node(get_node(parent), name);
}

Ogre::SceneNode*
Scene::create_child_node(Ogre::SceneNode* parent, std::string name) {
  Ogre::SceneNode* node = create_node(name);
  add_child(parent, node);

  return node;
}

Ogre::Entity*
Scene::create_entity(std::string name, std::string mesh, bool cast_shadows) {
  if(scene_manager_->hasEntity(name))
    return scene_manager_->getEntity(name);

  Ogre::Entity* entity = scene_manager_->createEntity(name, mesh);
  entity->setCastShadows(cast_shadows);
  return entity;
}

void
Scene::add_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->addChild(child);
}

void
Scene::move_node(std::string node_name, Ogre::Vector3 increment) {
  Ogre::SceneNode* node = get_node(node_name);

  Ogre::Vector3 position = node->getPosition();

  node->setPosition(position + increment);
}

Ogre::ParticleSystem*
Scene::get_particle(std::string name, std::string particle_system) {
  return get_particle(get_node(name), name, particle_system);
}

Ogre::ParticleSystem*
Scene::get_particle(Ogre::SceneNode* node, std::string name, std::string particle_system) {
  if(scene_manager_->hasParticleSystem(name))
    return scene_manager_->getParticleSystem(name);

  Ogre::ParticleSystem* particle =scene_manager_->createParticleSystem(name, particle_system);

  node->attachObject(particle);

  return particle;
}

Ogre::ParticleSystem*
Scene::get_particle(std::string name, std::string particle_system, Ogre::Vector3 position) {
  Ogre::SceneNode* node = get_child_node("", name);
  node->setPosition(position);

  return get_particle(node, name, particle_system);
}

void
Scene::destroy_node(std::string name) {
  if(scene_manager_->hasSceneNode(name))
    destroy_node(get_node(name));
}

void
Scene::destroy_node(Ogre::SceneNode* child) {
  destroy_all_attached_movable_objects(child);
  scene_manager_->destroySceneNode(child);
}

void
Scene::destroy_entity(Ogre::Entity* entity) {
  scene_manager_->destroyEntity(entity);
}


void
Scene::remove_child(std::string parent, std::string child) {
  remove_child(get_node(parent), get_node(child));
}

void
Scene::remove_child(std::string parent, Ogre::SceneNode* child) {
  remove_child(get_node(parent), child);
}

void
Scene::remove_child(Ogre::SceneNode* parent, std::string child) {
  remove_child(parent, get_node(child));
}

void
Scene::remove_child(Ogre::SceneNode* parent, Ogre::SceneNode* child) {
  parent->removeChild(child);
}

void
Scene::destroy_scene() {
  destroy_all_attached_movable_objects(get_node(""));
  get_node("")->removeAndDestroyAllChildren();
}

void
Scene::destroy_all_attached_movable_objects(Ogre::SceneNode* node) {
    for(auto object: node->getAttachedObjectIterator())
       scene_manager_->destroyMovableObject(object.second);

    for(auto child: node->getChildIterator())
        destroy_all_attached_movable_objects(
			  static_cast<Ogre::SceneNode*>(child.second));
}

Ogre::Vector3
Scene::convert_btvector3_to_vector3(btVector3 position){
  return Ogre::Vector3(position.getX(), position.getY(), position.getZ());
}

Ogre::Vector3
Scene::get_axis(std::string axis){
  if(axis == "X")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_Y;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Z;

  return Ogre::Vector3::UNIT_X;
}

Ogre::Vector3
Scene::get_normal(std::string axis) {
  if(axis == "X")
    return Ogre::Vector3::UNIT_Z;

  if(axis == "Y")
    return Ogre::Vector3::UNIT_X;

  if(axis == "Z")
    return Ogre::Vector3::UNIT_Y;

  return Ogre::Vector3::UNIT_X;
}

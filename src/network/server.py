#!/usr/bin/python -u
# -*- coding:utf-8; tab-width:4; mode:python -*-
# Copyright (C) 2014  ISAAC LACOBA MOLINA
# Tinman author: Isaac Lacoba Molina

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import Ice
Ice.loadSlice('network/tinman.ice -I/usr/share/Ice-3.5.1/slice')
import Tinman

class AuthoritativeServer(Tinman.AuthoritativeServer):
    peers = []
    n_players = 0
    next_race = []
    NUMBER_PLAYERS = 2

    def notify(self, snapshot, current):

        for car_info in snapshot:
            # print("ID: {0} Secuencia: {1} Orientation: {2} position: {3} velocity: {4} Action: {5}".format(car_info.carId, car_info.seqNumber, car_info.orientation, car_info.position, car_info.velocity, car_info.actions))
            sys.stdout.flush()
            self.send_to_other_peers(car_info, snapshot)

    def send_to_other_peers(self, car_info, snapshot):
        index = 0
        for peer in self.peers:
            if index != car_info.carId:
                peer[2].notify(list(snapshot))
            index = index + 1

    def login_async(self, cb, nickname, proxy, current=None):
        print("login: {0}".format(nickname))
        if([nick for nick in self.peers if nick[1] == nickname]):
            print "Nickname {0} already registered"
            nickname = nickname + str(len(self.peers))

        self.peers.append((cb, nickname, proxy))
        if len(self.peers) == self.NUMBER_PLAYERS:
            self.start_game()

    def start_game(self):
        id = -1
        playersNick = []
        for cb, nick, proxy in self.peers:
            playersNick.append(nick)

        for cb, nick, proxy in self.peers:
            print("enviando nick jugador{0}...".format(nick))
            id = id + 1
            login_info = Tinman.LoginInfo(id, playersNick)
            cb.ice_response(login_info)

    def startRace_async(self, cb, nickname, current=None):
        self.next_race.append(cb)

        if(len(self.next_race) == self.NUMBER_PLAYERS):
            self.start_new_race()

    def start_new_race(self):
        for cb in self.next_race:
            cb.ice_response()
        self.next_race = []

    def disconnect(self, player_id, current):
        print "disconecting ", player_id, "..."
        if len(self.peers) == 1:
            del self.peers[0]
            return

        del self.peers[player_id]

class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = AuthoritativeServer()

        adapter = broker.createObjectAdapter("AuthoritativeServerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("tinman"))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))

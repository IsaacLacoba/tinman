#ifndef __tinmanI_h__
#define __tinmanI_h__
#include <memory>

#include <tinman.h>

class Session;

namespace Tinman {

  class PeerI : virtual public Peer {
    std::shared_ptr<Session> session_;
  public:
    PeerI(std::shared_ptr<Session> session);
    virtual void notify(const ::Tinman::Snapshot&,
                        const Ice::Current&);
  };
}
#endif

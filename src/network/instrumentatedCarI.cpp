#include <instrumentatedCarI.h>

Tinman::InstrumentatedCarI::InstrumentatedCarI(std::string nick): Car(nick) {
  std::cout << "[Icar] constructor" << std::endl;
}

void
Tinman::InstrumentatedCarI::exec(::Tinman::Action action,
                                 const Ice::Current& current) {
  std::cout << "[Icar] exec: " << action << std::endl;
  Car::exec(action);
}

::Ice::Int
Tinman::InstrumentatedCarI::getNitro(const Ice::Current& current) {

    return Car::get_nitro();
}

::Ice::Int
Tinman::InstrumentatedCarI::getLap(const Ice::Current& current) {
    return Car::get_lap();
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getPosition(const Ice::Current& current) {
  btVector3 position = Car::get_position();
  return {position.getX(), position.getY(), position.getZ()};
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getVelocity(const Ice::Current& current) {
  return {Car::get_velocity().x(), Car::get_velocity().y(),
      Car::get_velocity().z()};
}

::Tinman::Quaternion
Tinman::InstrumentatedCarI::getOrientation(const Ice::Current& current) {
  return {Car::get_orientation().getX(),
      Car::get_orientation().getY(),
      Car::get_orientation().getZ(),
      Car::get_orientation().getW()};
}

::Tinman::Vector3
Tinman::InstrumentatedCarI::getDirection(::Ice::Int factor,
                                         const Ice::Current& current) {
  return {Car::get_direction().x(), Car::get_direction().y(),
      Car::get_direction().z()};
}

::Ice::Float
Tinman::InstrumentatedCarI::getSpeed(const Ice::Current& current) {
    return Car::get_speed();
}

bool
Tinman::InstrumentatedCarI::isColliding(const Ice::Current& current) {
  std::cout << "[ICar] " << __func__ << std::endl;
  return Car::is_colliding();
}

bool
Tinman::InstrumentatedCarI::isStuck(const Ice::Current& current) {
  std::cout << "[ICar] " << __func__ << std::endl;
  return is_stuck();
}


void
Tinman::InstrumentatedCarI::move(const ::Tinman::Vector3& position,
                                 const Ice::Current& current) {
  btVector3 new_position(position.x, position.y, position.z);
  set_position(new_position);
}
